
from tokenize import group
import app.database as database
import json
from urllib import response
import requests
from re import U
from flask import Flask, Response, make_response, Request, jsonify, send_file
from cas import CASClient
from flask import Flask, request, session, redirect, url_for, render_template, flash
import os
from flask_cors import CORS, cross_origin
import jwt
import datetime
import glob
import traceback

app = Flask(__name__)
app.secret_key = "testing"
cors = CORS(app, origins=["http://localhost:3999", "http://localhost:5173", "http://localhost:80",
            "https://projectdatabase.discovery.cs.vt.edu/", "https://havensprings.discovery.cs.vt.edu/", "https://projectdatabase.discovery.cs.vt.edu", "https://havensprings.discovery.cs.vt.edu"])
callbackURL = "http://localhost:5173" if os.getenv(
    'dev') is "1" else "https://projectdatabase.discovery.cs.vt.edu/"

if __name__ == "__main__":
    # use 0.0.0.0 to use it in container
    app.run(host='0.0.0.0')


cas_client = CASClient(
    version=3,
    service_url="https://havensprings.discovery.cs.vt.edu/login",
    server_url="https://login.cs.vt.edu/cas/login"
)
app.config['SESSION_COOKIE_SAMESITE'] = "None"
# relative directory for images
directory = "images/"

# full directory to images
p = os.path.join(os.getcwd(), directory)

# Helper function to help extract token from cookie (response)


def decodeToken(req):
    try:
        token = None

        for k, v in req.cookies.items():
            if k == "token":
                token = v

        if token is None:
            token = req.headers.get('X-Xsrf-Token')
            print("grabbing token from headers rather than cookie")
            print(token)

        if token is None:
            return None

        token = jwt.decode(jwt=token, key="secret", algorithms="HS256")

        return token
    except Exception as e:

        return None

# Helper function to check if cookie is valid (by checking with database entry)


def verifyCookieToUser(req):
    try:
        decoded = decodeToken(req)

        print("printing decoded")
        print(decoded)


        context = decoded['context']

        name = decoded['name']

        databaseEntry = database.getStudent(
            json.dumps({"pid": decoded['context']['pid']}))

        if databaseEntry['pid'] != context['pid'] or databaseEntry['role'] != context['role'] or databaseEntry['name'] != context['name']:
            return None

        if databaseEntry['name'] != name:
            return None

        return databaseEntry

    except Exception as e:
        return None


@app.route('/profile')
@cross_origin()
def profile(method=['GET']):
    app.logger.debug(request)
    resp = make_response("why")
    resp.headers.add("Access-Control-Allow-Origin", "*")
    return resp
    if 'username' in session:
        return 'Logged in as %s. <a href="/logout">Logout</a>' % session['username']
    return 'Login required. <a href="/login">Login</a>', 403


@app.route('/testing', methods=(['GET']))
@cross_origin(supports_credentials=True)
def testing():

    decoded = decodeToken(request)

    if decoded is not None:
        resp = make_response({
            "data": 'You are a {}'.format(decoded['context']['role'])
        })
        return resp

    return 'Error', 404


@app.route('/login')
@cross_origin(supports_credentials=True)
def login():
    next = request.args.get('next')
    ticket = request.args.get('ticket')

    decoded = decodeToken(request)
    print("DECODED HERE")
    print(decoded)

    if os.getenv('dev') is "1" and decoded is None:
        me = database.getStudent(json.dumps({"pid": "ali"}))

        token = jwt.encode({
            "name": me['name'],
            "exp": datetime.datetime.now()+datetime.timedelta(days=1),
            "context": {
                "name": me['name'],
                "pid": me['pid'],
                "role": me['role']
            }
        }, "secret", algorithm="HS256")
        return redirect('{}?token={}'.format(callbackURL, token), code=307)

    if decoded is not None:
        verified = verifyCookieToUser(request)

        if verified is None:
            return redirect(callbackURL, code=307)

        token = jwt.encode({
            "name": verified['name'],
            "exp": datetime.datetime.now()+datetime.timedelta(days=1),
            "context": {
                "name": verified['name'],
                "pid": verified['pid'],
                "role": verified['role']
            }
        }, "secret", algorithm="HS256")

        app.logger.debug('{}?token={}'.format(callbackURL, token))

        # Already logged in
        # TODO Make changes here to solve redirect issue for local dev
        return redirect('{}?token={}'.format(next, token), code=307)

    if not ticket:
        # No ticket, the request come from end user, send to CAS login
        cas_login_url = cas_client.get_login_url()
        app.logger.debug('CAS login URL: %s', cas_login_url)
        app.logger.debug('next url: %s', next)

        return redirect(cas_login_url)

    # There is a ticket, the request come from CAS as callback.
    # need call `verify_ticket()` to validate ticket and get user profile.
    app.logger.debug('ticket: %s', ticket)
    app.logger.debug('next: %s', next)

    verified = cas_client.verify_ticket(ticket)

    user = verified[0]
    attributes = verified[1]

    # print("Hello")
    # print(user)

    app.logger.log(0, "user: %s", user)

    app.logger.debug(
        'CAS verify ticket response: user: %s, attributes: %s', user, attributes)

    if not user:
        return redirect('{}/#user-not-valid'.format(callbackURL), code=307)
    else:  # Login successfully, redirect according `next` query parameter.
        # session['username'] = user
        # session['attributes'] = attributes
        username = attributes['username']
        name = attributes['name']
        # username, email, name = attributes

        success = database.createStudent(json.dumps({
            "pid": username,
            "name": name
        }))

        token = jwt.encode({
            "name": name,
            "exp": datetime.datetime.now()+datetime.timedelta(days=1),
            "context": {
                "name": name,
                "pid": username,
                "role": success['role']
            }
        }, "secret", algorithm="HS256")
        app.logger.debug('{}?token={}'.format(callbackURL, token))
        return redirect('{}?token={}'.format(callbackURL, token), code=307)


@app.route('/logout')
def logout():
    redirect_url = url_for('logout_callback', _external=True)
    cas_logout_url = cas_client.get_logout_url(redirect_url)
    app.logger.debug('CAS logout URL: %s', cas_logout_url)
    # TODO Make changes here to solve redirect issue for local dev
    return redirect(cas_logout_url)


@app.route('/logout_callback')
def logout_callback():
    # redirect from CAS logout request after CAS logout successfully
    session.pop('username', None)
    return 'Logged out from CAS. <a href="/login">Login</a>'


@app.route('/updated')
@cross_origin()
def updated(method=['GET']):
    return "updated database"


# ~~~~~~~~~~ START OF ENDPOINT FUNCTIONS ~~~~~~~~~~

# Endpoint POST for creating project
# This also creates a new group with group name entered and students selected
# {
#     "projectName": "Sample nam123e",
#     "description": "Sample desc123ription",
#     "link": "Sample l123ink",
#     "students": [{"pid":"njunloy30", "name" : "James Junloy"}],
#     "groupName": "Sample 123group"
#     "semesterTag": "FALL 2022"
#     "techTags": ["React", "Flask"]
#     "videos": "https://youtube.com/watch?"
# }
@app.route('/createproject', methods=['POST'])
@cross_origin(supports_credentials=True)
def createProject():

    decoded = decodeToken(request)

    if decoded is None:
        return "Not found", 404

    if request.method == 'POST':

        jsonRequest = request.json
        projectNameInput = jsonRequest['projectName']
        descriptionInput = jsonRequest['description']
        linkInput = jsonRequest['link']
        studentsInput = jsonRequest['students']
        groupNameInput = jsonRequest['groupName']
        semesterTagInput = jsonRequest['semesterTag']
        techTagsInput = jsonRequest['techTags']
        videoInput = jsonRequest['videos']

        project = {
            "projectName": projectNameInput,
            "description": descriptionInput,
            "link": linkInput,
            "students": studentsInput,
            "groupName": groupNameInput,
            "semesterTag": semesterTagInput,
            "techTags": techTagsInput,
            "video": videoInput
        }

        projectJson = json.dumps(project, indent=4)

        newId = database.createProject(projectJson)

        return newId

    return "Error"

# Create a student endpoint (for adding student upon CAS login)


@app.route('/createstudent', methods=['POST'])
@cross_origin()
def createStudent():

    if request.method == 'POST':

        jsonRequest = request.json

        studentPID = jsonRequest['pid']
        studentName = jsonRequest['name']

        # student = {
        #     "pid": "fakepid@vt.edu",
        #     "name": "fakename"
        # }

        student = {
            "pid": studentPID,
            "name": studentName
        }

        studentJson = json.dumps(student, indent=4)

        database.createStudent(studentJson)

        return "Successfully created student and added to database."

    return "Error"


@app.route('/allstudents', methods=['GET'])
@cross_origin()
def getAllStudents():
    # resp = make_response()
    # resp.headers.add("Access-Control-Allow-Origin", "*")
    # gets all tags
    if request.method == 'GET':
        projectID = request.args.get('id')
        allStudents = database.getAllStudents() if not projectID else database.getAllStudentsWithProject(projectID)

        return allStudents

    return 'None', 400


@app.route('/alltags', methods=['GET'])
@cross_origin()
def alltags():
    # resp = make_response()
    # resp.headers.add("Access-Control-Allow-Origin", "*")
    # gets all tags
    if request.method == 'GET':
        allTags = database.getAllTags()

        return allTags

    return 'None', 400

# Tags endpoint for both adding and deleting TECH STACK tags to database


@app.route('/tags', methods=(['POST', 'DELETE']))
@cross_origin(supports_credentials=True)
def tags():
    print(request.cookies)
    print(request.headers)
    verified = verifyCookieToUser(request)

    if verified is None:
        return 'bad request', 401

    if verified['role'] != 'Admin':
        return 'bad request', 401

    if request.method == 'POST':

        tagJson = request.json
        tagName = tagJson["tag"]

        tag = {
            "tag": tagName
        }

        tagToAdd = json.dumps(tag, indent=4)

        allTechTags = database.createTechTag(tagToAdd)

        if (type(allTechTags) is not list):
            return 'bad request', 400

        return {
            "techTags": allTechTags
        }

    elif request.method == 'DELETE':
        tagJson = request.json
        tagName = tagJson["tag"]

        tag = {
            "tag": tagName
        }

        tagsToDelete = json.dumps(tag, indent=4)

        allTechTags = database.deleteTechTag(tagsToDelete)

        if (type(allTechTags) is not list):
            return 'bad request', 400

        return {
            "techTags": allTechTags
        }

    return "Error", 400

# Tags endpoint for both adding and deleting SEMESTER tags to database


@app.route('/semestertags', methods=['POST', 'DELETE'])
@cross_origin(supports_credentials=True)
def semesterTags():

    verified = verifyCookieToUser(request)

    if verified is None:
        return 'bad request', 401

    if verified['role'] != 'Admin':
        return 'bad request', 401

    if request.method == 'POST':
        semesterTagJson = request.json
        semesterName = semesterTagJson["semester"]

        semesterTag = {
            "semester": semesterName
        }

        semesterTagToAdd = json.dumps(semesterTag, indent=4)

        allSemesterTags = database.createSemesterTag(semesterTagToAdd)

        if (type(allSemesterTags) is not list):
            return 'Bad request!', 400

        return {
            "semesterTags": allSemesterTags
        }

    elif request.method == 'DELETE':
        semesterTagJson = request.json
        semesterName = semesterTagJson["semester"]

        semesterTag = {
            "semester": semesterName
        }

        semesterTagToDelete = json.dumps(semesterTag, indent=4)

        allSemesterTags = database.deleteSemesterTag(semesterTagToDelete)

        if (type(allSemesterTags) is not list):
            return 'Bad request!', 400

        return {
            "semesterTags": allSemesterTags
        }

    return "Error", 400

# Get the tech tags of a project given project id


@app.route('/techtagproject', methods=['POST'])
@cross_origin()
def addTechTagToProject():

    if request.method == 'POST':

        jsonRequest = request.json
        project_id = jsonRequest['project_id']
        techtag = jsonRequest['tag']

        jsonObj = {
            "project_id": project_id,
            "tag": techtag,
        }

        tagToAdd = json.dumps(jsonObj, indent=4)

        database.addTag(tagToAdd)

        return "Successfully added tech tag to project."

    return "Error"

# Get the semester tags of a project given project id


@app.route('/semestertagproject', methods=['POST'])
@cross_origin()
def addSemesterTagToProject():

    if request.method == 'POST':

        jsonRequest = request.json
        project_id = jsonRequest['project_id']
        techtag = jsonRequest['tag']

        jsonObj = {
            "project_id": project_id,
            "tag": techtag,
        }

        tagToAdd = json.dumps(jsonObj, indent=4)

        database.addSemesterTag(tagToAdd)

        return "Successfully added semester tag to project."

    return "Error"

# Get all tags on a project given project id


@app.route('/getprojecttags/', methods=['GET'])
@cross_origin()
def getProjectTags():

    if request.method == 'GET':
        getJson = request.json
        projectID = getJson["project_id"]

        id = {
            "project_id": projectID,
        }
        jsonObj = json.dumps(id, indent=4)
        foundProjectTags = database.getProjectTags(jsonObj)

        return foundProjectTags

# Get a project by specific id endpoint
# Returns a JSON of a project


@app.route('/getprojectwithid/<int:id>', methods=['GET'])
@cross_origin()
def getProjectWithID(id):

    if request.method == 'GET':
        # getJson = request.json
        # projectID = getJson["id"]

        foundProject = database.getProject(id)

        print("Found: ")
        print(foundProject)

        return foundProject

# Get all projects endpoint
# Returns a JSON of all projects


@app.route('/projects', methods=['GET'])
@cross_origin()
def getAllProjects():

    if request.method == 'GET':
        pageNum = request.args.get('page')

        page = None
        if pageNum != None:
            page = {
                "page": pageNum
            }
        else:
            page = {
                "page": 0
            }

        pageToSearch = json.dumps(page, indent=4)

        foundProject = database.getPage(pageToSearch)

        print("Found: ")
        print(foundProject)

        return foundProject

    return None, 400

# Get projects by filter endpoint
# Returns a JSON of all projects filtered


@app.route('/filterprojects', methods=['POST'])
@cross_origin()
def getFilteredProjects():

    if request.method == 'POST':
        getJson = request.json

        page = request.args.get("page")
        tagFilter = getJson.get("tags") if getJson.get("tags") else []
        semesterFilter = getJson.get(
            "semester") if getJson.get("semester") else []
        textFilter = getJson.get("text") if getJson.get("text") else None

        j = {
            "page": page,
            "tags": tagFilter,
            "semester": semesterFilter,
            "name": textFilter
        }

        jsonObj = json.dumps(j, indent=4)

        foundProject = database.getProjectsByFilter(jsonObj)

        print("Found: ")
        print(foundProject)

        return foundProject

    return None, 400

# Delete a project by specific id endpoint


@app.route('/deleteproject/<int:id>', methods=(['DELETE']))
@cross_origin(supports_credentials=True)
def deleteProjectWithID(id):

    if request.method == 'DELETE':
        projectID = id

        database.deleteProject(projectID)

        print("Successfully deleted project with ID: " + str(projectID))

        return "Successfully deleted project with ID: " + str(projectID)

# Create a comment for a project
@app.route('/makecomment', methods=(['POST']))
@cross_origin()
def makeComment():
    if request.method == 'POST':
        getJson = request.json
        pid = getJson["pid"]
        text = getJson["text"]
        projectID = getJson["projectID"]

        c = {
            "pid": pid,
            "text": text,
            "projectID": projectID
        }

        jsonObj = json.dumps(c, indent=4)

        createdId = database.createComment(jsonObj)

        print("Successfully created comment for project with ID: ")
        print(projectID)

        return createdId

# Get all comments for the project id given
@app.route('/projects/<int:id>/comments', methods=(['GET']))
@cross_origin(supports_credentials=True)
def getCommentsForProject(id):
    if request.method == 'GET':
        comments = database.getComment(json.dumps({ "projectID" : id }, indent = 4))

        return comments

# Delete the specified comments by id
@app.route('/comments', methods=(['DELETE']))
@cross_origin()
def deleteCommentsById():
    if request.method == 'DELETE':
        inputJson = request.json
        idsToDelete = inputJson["ids"]
        remainingComments = []
        for id in idsToDelete:
            remainingComments = database.deleteComment(id)
            print("Successfully deleted comment with id: ")
            print(id)

        print(remainingComments)
        return remainingComments

# NOTES ENDPOINTS
@app.route('/notes', methods=(['POST', 'DELETE']))
@cross_origin()
def createOrDeleteNote():
    if request.method == 'POST':
        getJson = request.json
        text = getJson["text"]
        projectID = getJson["projectID"]

        n = {
            "text": text,
            "projectID": projectID
        }

        jsonObj = json.dumps(n, indent=4)

        createdId = database.createNote(jsonObj)

        print("Successfully created note for project with ID: ")
        print(projectID)

        return createdId
    if request.method == 'DELETE':
        inputJson = request.json
        print(inputJson)
        idsToDelete = inputJson["ids"]
        remainingNotes = []
        print(idsToDelete)
        for id in idsToDelete:
            remainingNotes = database.deleteNote(id)
            print("Successfully deleted note with id:")
            print(id)
        print(remainingNotes)
        return remainingNotes

@app.route('/getNotesForProject/<int:id>', methods=(['GET']))
@cross_origin(supports_credentials=True)
def getNotesForProject(id):
    if request.method == 'GET':
        print("get notes for project")
        notes = database.getNote(json.dumps({ "projectID" : id }, indent = 4))

        return notes

# @app.route('/notes', methods=(['DELETE']))
# @cross_origin()
# def deleteNotesById():
#     if request.method == 'DELETE':


# Delete a project by specific id endpoint
# @app.route('/uploadimage/', methods=(['POST']))
# @cross_origin()
# def uploadImage():

#     directory = "/images"

#     if request.method == 'POST':

#         getJson = request.json

#         files = getJson["files"]

#         for aFile in files:
#             # img = {
#             #     "name": aFile["name"],
#             #     "size": aFile["size"],
#             #     "type": aFile["type"]
#             # }

#             aFile.save(os.path.join(directory, aFile["name"]))

#             # file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

#         return "Saved images"

#     return "Error", 400


@app.route('/studentgroup', methods=(['GET']))
@cross_origin(supports_credentials=True)
def studentGroup():
    verified = verifyCookieToUser(request)

    print("here")
    print("here")
    print("here")
    print("here")
    print("here")
    print("here")
    print("here")
    print("here")
    print("here")
    print("here")
    print(request.headers)
    print(verified)
    print(request.cookies)

    if verified is None:
        return 'bad request', 401

    if request.method == 'GET':
        result = database.getStudentProject(json.dumps({"pid": verified['pid']}))

        return {
            "group": result
        }

    return 'bad request', 400

@app.route('/editproject', methods=(['POST']))
@cross_origin(supports_credentials=True)
def editProjectMethod():
    verified = verifyCookieToUser(request)
    projectID = request.args.get("id")

    if verified is None or projectID is None:
        return 'bad request', 401

    if request.method == 'POST':
        proj = database.getProject(projectID)

        jsonRequest = request.json

        projectNameInput = jsonRequest['projectName']
        descriptionInput = jsonRequest['description']
        linkInput = jsonRequest['link']
        studentsInput = jsonRequest['students']
        groupNameInput = jsonRequest['groupName']
        semesterTagInput = jsonRequest['semesterTag']
        techTagsInput = jsonRequest['techTags']
        videoInput = jsonRequest['videos']
        id = jsonRequest['id']

        project = {
            "id": id,
            "projectName": projectNameInput,
            "description": descriptionInput,
            "link": linkInput,
            "students": studentsInput,
            "groupName": groupNameInput,
            "semesterTag": semesterTagInput,
            "techTags": techTagsInput,
            "video": videoInput
        }

        projectJson = json.dumps(project, indent=4)


        if proj is None:
            return 'not found', 404

        if verified['role'] == 'Admin':
            finalRes = database.editProject(projectJson)
            return {
                "group": finalRes
            }

        if verified['role'] =='Student':
            studentProj = database.getStudentProject(json.dumps({"pid": verified['pid']}))
            if (str(studentProj['id']) == str(proj['id'])) and (studentProj['projectName'] == proj['projectName']):
                finalRes = database.editProject(projectJson)
                # print(studentProj)
                # finalRes = database.editGroup(json.dumps({
                #      "id" : studentProj.get("id"),
                #     "groupName" : studentProj.get("groupName"),
                #     "students":studentProj.get("students")
                # }))
                return {
                    "group": finalRes
                }


    return 'bad request', 400

@app.route('/upload', methods=(['POST']))
@cross_origin(supports_credentials=True)
def uploadImage():
    verified = verifyCookieToUser(request)
    projectID = request.args.get("id")

    if verified is None:
        return 'bad request', 401

    if request.method == 'POST':

        if verified['role'] == 'Admin' and projectID != None:
            proj = database.getProject(projectID)
            if proj is None:
                return 'bad request', 404
            id = proj['id']

            files = [request.files[i] for i in request.files]
            toKeep = []
            # delete old
            for i in glob.glob(os.path.join(p,'project-{}*').format(id)):
                exists = False
                temp = None
                for x in files:
                    if x.filename in i:
                        exists = True
                        temp = x
                if exists is False:
                    try:
                        os.remove(i)
                        print("removed " + i)
                    except Exception as e:
                        print(e)
                else:
                    toKeep.append(temp)

            # add new
            for i in files:
                exist = False
                for x in toKeep:
                    if i.filename == x.filename:
                        exist = True
                if exist is False:
                    currF = i

                    finalFilePath = None


                    finalFilePath = os.path.join(p, 'project-{}-{}'.format(id,currF.filename))
                    print(finalFilePath)
                    currF.save(finalFilePath)
            return 'success', 200
            # finalRes = database.editProject(projectJson)
            # return {
            #     "group": finalRes
            # }

        if verified['role'] =='Student':
            proj = database.getStudentProject(json.dumps({"pid": verified['pid']}))
            if proj is None:
                return 'bad request', 404

            id = proj['id']

            files = [request.files[i] for i in request.files]
            toKeep = []
            # delete old
            for i in glob.glob(os.path.join(p,'project-{}*').format(id)):
                exists = False
                temp = None
                for x in files:
                    if x.filename in i:
                        exists = True
                        temp = x
                if exists is False:
                    try:
                        os.remove(i)
                        print("removed " + i)
                    except Exception as e:
                        print(e)
                else:
                    toKeep.append(temp)

            # add new
            for i in files:
                exist = False
                for x in toKeep:
                    if i.filename == x.filename:
                        exist = True
                if exist is False:
                    currF = i

                    finalFilePath = None


                    finalFilePath = os.path.join(p, 'project-{}-{}'.format(id,currF.filename))
                    print(finalFilePath)
                    currF.save(finalFilePath)
            return 'success',200
            # print(request.files)


    return 'bad request', 400

@app.route('/imagelist', methods=(['GET']))
@cross_origin()
def imageList():

    projectID = request.args.get("id")


    if projectID is None:
        return 'bad request', 401

    if request.method == 'GET':
        proj = database.getProject(projectID)
        if proj is None:
            return 'bad request', 404
        id = proj['id']

        filesNamesToReturn = []

        for i in glob.glob(os.path.join(p,'project-{}*').format(id)):
            filesNamesToReturn.append({
                "name": os.path.basename(i)
            })

        return {
            "images": filesNamesToReturn
        }


    return 'bad request', 400

@app.route('/image/<img>', methods=(['GET']))
@cross_origin()
def publicImage(img):
    return send_file(os.path.join(p, img))
