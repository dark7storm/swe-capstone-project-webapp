import json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy import Column, ForeignKey, Integer, Table, String, DateTime
from sqlalchemy import create_engine
from sqlalchemy import or_
from sqlalchemy.sql import func
import datetime
import os
import glob


db = declarative_base()
#add semester and unique id
class student(db):
    __tablename__ = "student"
    id = Column('id', Integer, primary_key = True)
    pid = Column('pid', String(256))
    name = Column('name', String(200))
    groupID = Column("group_id", Integer, ForeignKey("group.id"))
    role = Column("role", String(16))

class group(db):
    __tablename__ = "group"
    id = Column('id', Integer, primary_key = True)
    groupName = Column('group_name', String(256))
    members = relationship("student", backref="group")
    projectID = Column("project_id", Integer, ForeignKey("project.id"))

class project(db):
    __tablename__ = "project"

    # Attributes
    id = Column('id', Integer, primary_key = True)
    projectName = Column('projectName', String(256))
    description = Column('description', String(5000))
    link = Column('link', String(256))
    video = Column('video', String(256))

    # Relations
    groupid = relationship("group", backref="project")
    comments = relationship("comment", backref="project")
    notes = relationship("note", backref="project")
    tags = relationship("projectTag", backref="project")
    semester = relationship("semesterProjectTag", backref="project")

class tag(db):
    __tablename__ = "tags"
    id = Column('id', Integer, primary_key = True)
    tag = Column('tag', String(100))

class projectTag(db):
    __tablename__ = "projectTag"
    id = Column('id', Integer, primary_key = True)
    projectName = Column('project_id', Integer, ForeignKey("project.id"))
    tag_id = Column('tag_id', Integer, ForeignKey("tags.id"))

class semester_tag(db):
    __tablename__ = "semester_tag"
    id = Column('id', Integer, primary_key = True)
    tag = Column('semester', String(100))

class semesterProjectTag(db):
    __tablename__ = "semesterProjectTag"
    id = Column('id', Integer, primary_key = True)
    projectName = Column('project_id', Integer, ForeignKey("project.id"))
    tag_id = Column('tag_id', Integer, ForeignKey("semester_tag.id"))

class comment(db):
    __tablename__ = "comments"
    id = Column('id', Integer, primary_key = True)
    student = Column('pid', String(256))
    text = Column('text', String(5000))
    projectID = Column("projectID", Integer, ForeignKey("project.id"))

class note(db):
    __tablename__ = "notes"
    id = Column('id', Integer, primary_key = True)
    text = Column('text', String(5000))
    projectID = Column("projectID", Integer, ForeignKey("project.id"))

# mysql://root:65324858Jj!@localhost:3306/treyDB
# TODO change this back to prevs
DB_URL = "mysql://root:Password123!@localhost:3306/treyDB" if os.getenv('dev') is "1" else "mysql://root:bannedfromwafflehouse@10.42.27.41:3306/treyDB"
engine = create_engine(
    DB_URL, echo=True, future=True, pool_pre_ping=True)
db.metadata.create_all(engine)



def getAllTags():
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        techTags = session.query(tag).all()
        semesterTags = session.query(semester_tag).all()

        return {
            "techTags": [i.tag for i in techTags],
            "semesterTags": [i.tag for i in semesterTags]
        }


# Get project by specified project ID
# Returns a JSON of a project
def getProject(idNum):
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        result = session.query(project).filter_by(id = idNum).first()
        groupResult = session.query(group).filter_by(projectID = result.id).first()
        x = {
            "projectName": result.projectName,
            "id": idNum,
            "description": result.description,
            "link": result.link,
            "groupName" : groupResult.groupName,
            "groupID" : groupResult.id,
            "students" : [],
            "techTags": [],
            "semester": "",
            "video": result.video
        }
        students = list(session.query(student).filter_by(groupID = groupResult.id))
        studentResult = []
        for i in range(0, len(students)):
            studentResult.append({"pid": students[i].pid, "name" : students[i].name, "group" : groupResult.groupName})
        x['students'] = studentResult

        tagLinks = session.query(projectTag).filter_by(projectName = idNum)

        # Project Tech Tags
        for a in list(tagLinks.all()):
            tagTarget = session.query(tag).filter_by(id = a.tag_id).first()
            x.get("techTags").append(tagTarget.tag)

        # Project Semester Tag
        semesters = session.query(semesterProjectTag).filter_by(projectName = idNum)
        for a in list(semesters.all()):
            tagTarget = session.query(semester_tag).filter_by(id = a.tag_id).first()
            x["semester"] = tagTarget.tag

        filesNamesToReturn = []

        # relative directory for images
        directory = "images/"

        # full directory to images
        p = os.path.join(os.getcwd(), directory)

        for i in glob.glob(os.path.join(p,'project-{}*').format(groupResult.id)):
            filesNamesToReturn.append(os.path.basename(i))
        x['images'] = filesNamesToReturn

        return x

def deleteProject(idNum):
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        result = session.query(project).filter_by(id = idNum).first()
        gResult = session.query(group).filter_by(projectID = idNum).first()
        session.delete(result)
        session.delete(gResult)
        session.commit()

        return result

def createTechTag(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        if(session.query(tag).filter_by(tag = dictionary.get("tag")).first() is not None):
            print("Tech tag already exists")
            return "Tech tag already exists"

        tempTag = tag(tag = dictionary.get("tag"))
        session.add(tempTag)
        session.commit()

        return [i.tag for i in session.query(tag).all()]

def deleteTechTag(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:

        tagList = dictionary.get('tag')

        for currTag in tagList:
            if(session.query(tag).filter_by(tag = currTag).first() is not None):
                print("Tech tag exists, deleting")
                teg = session.query(tag).filter_by(tag = currTag).first()
                session.delete(teg)
                session.commit()

        return [i.tag for i in session.query(tag).all()]


def createSemesterTag(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        if(session.query(semester_tag).filter_by(tag = dictionary.get("semester")).first() is not None):
            print("Semester tag already exists")
            return "Semester tag already exists"

        tempTag = semester_tag(tag = dictionary.get("semester"))
        session.add(tempTag)
        session.commit()

        return [i.tag for i in session.query(semester_tag).all()]

def deleteSemesterTag(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:

        tagList = dictionary.get('semester')

        for currTag in tagList:
            if(session.query(semester_tag).filter_by(tag = currTag).first() is not None):
                print("Semester tag exists, deleting")
                tag = session.query(semester_tag).filter_by(tag = currTag).first()
                session.delete(tag)
                session.commit()


        return [i.tag for i in session.query(semester_tag).all()]


def createStudent(studentDump):
    dictionary = json.loads(studentDump)

    from sqlalchemy.orm import Session

    with Session(engine) as session:
        if(session.query(student).filter_by(pid = dictionary.get("pid")).first() is not None):
                print("student already exists")
                return session.query(student).filter_by(pid = dictionary.get("pid")).first().__dict__

        tempStudent = student(pid = dictionary.get("pid"), name = dictionary.get("name"), role = "Student")
        session.add(tempStudent)
        session.commit()
        return session.query(student).filter_by(pid = dictionary.get("pid")).first().__dict__

def getStudent(studentDump):
    dictionary = json.loads(studentDump)

    from sqlalchemy.orm import Session
    with Session(engine) as session:
        result = session.query(student).filter_by(pid = dictionary.get("pid")).first()
        if(result is not None):
            return result.__dict__

        return None

def getAllStudents():
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        students = session.query(student).filter(or_(student.groupID == None, student.groupID == 0)).filter(student.role != "Admin")

        studentList = {"students" : []}

        for x in list(students):
            studentList.get("students").append({"pid": x.pid, "name":x.name})

        return studentList

def getAllStudentsWithProject(id):
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        students = session.query(student).filter(or_(student.groupID == None, student.groupID == id)).filter(student.role != "Admin")

        studentList = {"students" : []}

        for x in list(students):
            studentList.get("students").append({"pid": x.pid, "name":x.name})

        return studentList


def createProject(jsonDump):
    dictionary = json.loads(jsonDump)

    from sqlalchemy.orm import Session

    with Session(engine) as session:
        if(session.query(project).filter_by(projectName = dictionary.get("projectName")).first() is not None):
            return "project name is taken"
        tempProject = project(projectName = dictionary.get("projectName"), description = dictionary.get("description"), link = dictionary.get("link"), video = dictionary.get("video"))
        if(session.query(group).filter_by(groupName = dictionary.get("groupName")).first() is not None):
            return "group name is taken"

        session.add_all([tempProject])
        session.commit()

        tempGroup = group(groupName = dictionary.get("groupName"), projectID = tempProject.id)

        session.add_all([tempGroup])
        session.commit()

        for x in dictionary.get("techTags"):
            addTag(json.dumps({"project_id":str(tempProject.id), "tag":x}, indent = 4))

        addSemesterTag(json.dumps({"project_id":str(tempProject.id), "tag":dictionary.get("semesterTag")}, indent = 4))

        for x in dictionary.get("students"):
            print(x)
            if(session.query(student).filter_by(pid = x).first() is None):
                return "Student does not exist"
            session.query(student).filter_by(pid = x).update({student.groupID : tempGroup.id})
            session.commit()

        session.commit()

        newId = {
            "id": tempProject.id,
        }

        return newId

def getAllProjects():
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        result = session.query(project)
        projectList = {"projects" : []}
        for x in list(result.all()):
            groupResult = session.query(group).filter_by(projectID = x.id).first()
            y = {
                "projectName": x.projectName,
                "id": x.id,
                "description": x.description,
                "link": x.link,
                "groupName" : groupResult.groupName if groupResult.groupName else None,
                "students" : []
            }
            students = list(session.query(student).filter_by(groupID = groupResult.id))
            for i in range(0, len(students)):
                y.get("students").append({"pid": students[i].pid, "name" : students[i].name, "group" : groupResult.groupName})

            projectList.get("projects").append(y)

        return projectList

def addTag(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        tagTarget = session.query(tag).filter_by(tag = dictionary.get("tag")).first()
        print(session.query(projectTag).filter_by(projectName = dictionary.get("project_id")).filter_by(tag_id = tagTarget.id).first())
        print("TAG TARGET")
        print(tagTarget)
        if(session.query(projectTag).filter_by(projectName = dictionary.get("project_id")).filter_by(tag_id = tagTarget.id).first() is None):
            tempTagConnect = projectTag(projectName = dictionary.get("project_id"), tag_id = tagTarget.id)
            session.add(tempTagConnect)
            session.commit()

def addSemesterTag(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        tagTarget = session.query(semester_tag).filter_by(tag = dictionary.get("tag")).first()
        if(session.query(semesterProjectTag).filter_by(projectName = dictionary.get("project_id")).filter_by(tag_id = tagTarget.id).first() is None):
            tempTagConnect = semesterProjectTag(projectName = dictionary.get("project_id"), tag_id = tagTarget.id)
            session.add(tempTagConnect)
            session.commit()

# class comment(db):
#     __tablename__ = "comments"
#     id = Column('id', Integer, primary_key = True)
#     student = Column('pid', String(256))
#     text = Column('text', String(256))
#     projectID = Column("projectID", Integer, ForeignKey("project.id"))

def createComment(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:

        tempComment = comment(student = dictionary.get("pid"), text = dictionary.get("text"),
            projectID = dictionary.get("projectID"))

        session.add(tempComment)
        session.commit()

        # return id of newly created comment
        createdComment = session.query(comment).filter_by(student = dictionary.get("pid"), text = dictionary.get("text"),
            projectID = dictionary.get("projectID")).first()
        return {
            "id": createdComment.id
        }

def getComment(jsonDump):
    dictionary = json.loads(jsonDump)
    commentList = {"comments" : []}

    from sqlalchemy.orm import Session
    with Session(engine) as session:
        result = list(session.query(comment).filter_by(projectID = dictionary.get("projectID")))
        for x in (result):

            aComment = {
                "id": x.id,
                "pid": x.student,
                "text": x.text
            }

            commentList["comments"].append(aComment)

        print("Found comments")
        print(commentList)
        return commentList

def deleteComment(idNum):
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        print("deletecomment")
        print(idNum)
        result = session.query(comment).filter_by(id = idNum).first()
        print(result)
        projectid = result.projectID
        session.delete(result)
        session.commit()

        return getComment(json.dumps({"projectID" : projectid}, indent = 4))

# NOTES

def createNote(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:

        tempNote = note(text = dictionary.get("text"),
            projectID = dictionary.get("projectID"))

        session.add(tempNote)
        session.commit()

        # return id of newly created note
        createdNote = session.query(note).filter_by(text = dictionary.get("text"),
            projectID = dictionary.get("projectID")).first()
        return {
            "id": createdNote.id
        }

def getNote(jsonDump):
    dictionary = json.loads(jsonDump)
    noteList = {"notes" : []}

    from sqlalchemy.orm import Session
    with Session(engine) as session:
        result = list(session.query(note).filter_by(projectID = dictionary.get("projectID")))
        for x in (result):

            aNote = {
                "id": x.id,
                "text": x.text
            }

            noteList["notes"].append(aNote)

        print("Found notes")
        print(noteList)
        return noteList

def deleteNote(idNum):
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        print("deletecheck")
        print(idNum)
        result = session.query(note).filter_by(id = idNum).first()
        print("result")
        print(result.projectID)
        projectId = result.projectID
        session.delete(result)
        session.commit()

        return getNote(json.dumps({"projectID" : projectId}, indent = 4))


def testtest():
    print("This function is called")


# c = {
#     "pid": "njunloy30",
#     "text": "This is a test comment",

#     "projectID": "1"

# }

# testcomment = json.dumps(c, indent = 4)

# createComment(testcomment)


def getProjectTags(jsonDump):
    dictionary = json.loads(jsonDump)
    from sqlalchemy.orm import Session

    with Session(engine) as session:
        tagLinks = session.query(projectTag).filter_by(projectName = dictionary.get("project_id"))
        tags = []
        semestersList = []
        for x in list(tagLinks.all()):
            tagTarget = session.query(tag).filter_by(id = x.tag_id).first()
            tags.append(tagTarget.tag)

        semesters = session.query(semesterProjectTag).filter_by(projectName = dictionary.get("project_id"))
        for x in list(semesters.all()):
            tagTarget = session.query(semester_tag).filter_by(id = x.tag_id).first()
            semestersList.append(tagTarget.tag)
        return {"tags" : tags, "semesters" : semestersList}



#needs to be given a json containing an the page number, if nothing is given, returns page 0, return item formated as follows
#{
#    "page": "your page number",
#    "max_pages": "how many pages in the database",
#    "projects" : "projects in the page, stored in an array, same as getallprojects"
#}
def getPage(pagenumJson = None):
    dictionary = json.loads(pagenumJson)
    pagenum = dictionary.get("page")

    from sqlalchemy.orm import Session

    with Session(engine) as session:
        projectList = []
        page = 0
        items = int(session.query(project).count() / 8)
        if(pagenum != None):
            page = int(pagenum)
        result = session.query(project).order_by(project.id).offset(page * 8).limit(8)
        for x in list(result):
            groupResult = session.query(group).filter_by(projectID = x.id).first()
            y = {
                "projectName": x.projectName,
                "id": x.id,
                "description": x.description,
                "link": x.link,
                "groupName" : groupResult.groupName,
                "techTags": [],
                "semesterTag": None
            }

            techTags = [i.__dict__['tag_id'] for i in session.query(projectTag).filter(projectTag.projectName == x.id).all()]
            y['techTags'] = [i.__dict__['tag'] for i in session.query(tag).filter(tag.id.in_(techTags)).all()]

            semesterTag =  session.query(semesterProjectTag).filter(semesterProjectTag.projectName == x.id).first().__dict__['tag_id']
            y['semesterTag'] = session.query(semester_tag).filter(semester_tag.id == semesterTag).first().__dict__['tag']
            # y.get('tags').append()
            # y['semesterTags'] = session.query(semesterProjectTag).filter(semesterProjectTag.projectName == x.id).first().__dict__['']
            # students = list(session.query(student).filter_by(groupID = groupResult.id))
            # for i in range(0, len(students)):
            #     y.get("students").append({"pid": students[i].pid, "name" : students[i].name,
            #         "group" : groupResult.groupName})
            projectList.append(y)

        return {"page" : pagenum, "max_pages" : items, "per_page" : 8, "projects" : projectList}

def getProjectsByFilter(jsonDump):
    dictionary = json.loads(jsonDump)
    pagenum = dictionary.get("page")
    from sqlalchemy.orm import Session

    with Session(engine) as session:

        items = int(session.query(project).count() / 8)
        if(pagenum != None):
            page = int(pagenum)

        techTags = session.query(project.id)
        semesterTags = session.query(project.id)

        tagArray = []
        for x in dictionary.get("tags"):
            tagTarget = session.query(tag).filter_by(tag = x).first()
            tagArray.append(tagTarget.id)

        semesterTagTarget = []
        for x in dictionary.get('semester'):
            curr= session.query(semester_tag).filter_by(tag = x).first()
            semesterTagTarget.append(curr.id)

        res = None
        if len(tagArray) > 0 and len(semesterTagTarget) > 0:
            techTags = techTags.join(projectTag).filter(projectTag.tag_id.in_(tagArray)).group_by(project.id).having(func.count(project.id) == len(tagArray))
            techTags = [i[0] for i in techTags.all()]
            semesterTags = semesterTags.join(semesterProjectTag).filter(semesterProjectTag.tag_id.in_(semesterTagTarget)).filter(semesterProjectTag.projectName.in_(techTags)).group_by(project.id)
            res = semesterTags
        elif len(tagArray) > 0:
            techTags = techTags.join(projectTag).filter(projectTag.tag_id.in_(tagArray)).group_by(project.id).having(func.count(project.id) == len(tagArray))
            res = techTags
        elif len(semesterTagTarget) > 0:
            semesterTags = semesterTags.join(semesterProjectTag).filter(semesterProjectTag.tag_id.in_(semesterTagTarget)).group_by(project.id)
            res = semesterTags
        else:
            res = session.query(project.id)

        if dictionary.get("name") != "" and dictionary.get("name") is not None:
            res = res.filter(func.lower(project.projectName.contains(dictionary.get("name").lower())))

        projectList = []
        totalLen = res.count()
        result = res.offset(page * 8).limit(8)
        for q in list(result):
            x = session.query(project).filter_by(id = q.id).first()
            groupResult = session.query(group).filter_by(projectID = x.id).first()
            y = {
                "projectName": x.projectName,
                "id": x.id,
                "description": x.description,
                "link": x.link,
                "groupName" : groupResult.groupName,
                "techTags": [],
                "semesterTag": None,
                "images": []
            }
            techTags = [i.__dict__['tag_id'] for i in session.query(projectTag).filter(projectTag.projectName == x.id).all()]
            y['techTags'] = [i.__dict__['tag'] for i in session.query(tag).filter(tag.id.in_(techTags)).all()]

            semesterTag =  session.query(semesterProjectTag).filter(semesterProjectTag.projectName == x.id).first().__dict__['tag_id']
            y['semesterTag'] = session.query(semester_tag).filter(semester_tag.id == semesterTag).first().__dict__['tag']
            filesNamesToReturn = []

            # relative directory for images
            directory = "images/"

            # full directory to images
            p = os.path.join(os.getcwd(), directory)

            for i in glob.glob(os.path.join(p,'project-{}*').format(q.id)):
                filesNamesToReturn.append(os.path.basename(i))
            y['images'] = filesNamesToReturn
            projectList.append(y)

        return {"page" : pagenum, "max_pages" : int(totalLen / 8), "projects" : projectList}

# page = {
#     "page": "0",
#     "tags": ["React"],
#     "semester": ["Fall 2022"]
# }

# pageToSearch = json.dumps(page, indent = 4)

# print(getProjectsByFilter(pageToSearch))

def populate():

    # Create tech tags
    jsonTechTag1 = {
        "tag": "Python"
    }
    jsonTechTag2 = {
        "tag": "React"
    }

    techTag1 = json.dumps(jsonTechTag1, indent = 4)
    techTag2 = json.dumps(jsonTechTag2, indent = 4)
    createTechTag(techTag1)
    createTechTag(techTag2)

    # Create semester tags
    jsonSemesterTag1 = {
        "semester": "Fall 2022"
    }
    jsonSemesterTag2 = {
        "semester": "Fall 2021"
    }

    semesterTag1 = json.dumps(jsonSemesterTag1, indent = 4)
    semesterTag2 = json.dumps(jsonSemesterTag2, indent = 4)
    createSemesterTag(semesterTag1)
    createSemesterTag(semesterTag2)


def getStudentProject(studentDump):
    dictionary = json.loads(studentDump)

    from sqlalchemy.orm import Session
    with Session(engine) as session:
        result = session.query(student).filter_by(pid = dictionary.get("pid")).first()
        if(result is not None):
            finalRes = session.query(group).filter_by(id =result.__dict__['groupID']).first()
            if finalRes is not None:
                finalRes = session.query(project).filter_by(id = finalRes.__dict__['projectID']).first()
                if finalRes is not None:
                    finalRes = finalRes.__dict__
                    # print(finalRes)
                    del finalRes['_sa_instance_state']
                    return finalRes
                return None
            return None
        return None

def editProject(jsonDump):
    dictionary = json.loads(jsonDump)

    from sqlalchemy.orm import Session
    with Session(engine) as session:
        projectTarget = session.query(project).filter_by(id = dictionary.get("id"))
        if dictionary.get("projectName") != None:
            projectTarget.update({project.projectName : dictionary.get("projectName")})
        if dictionary.get("description") != None:
            projectTarget.update({project.description : dictionary.get("description")})
        if dictionary.get("link") != None:
            projectTarget.update({project.link : dictionary.get("link")})
        if dictionary.get("video") != None:
            projectTarget.update({project.video : dictionary.get("video")})

        session.commit()

        if dictionary.get("techTags") != None:
            tagLinks = session.query(projectTag).filter_by(projectName = dictionary.get("id"))
            for x in list(tagLinks.all()):
                session.delete(x)
            session.commit()

            for x in dictionary.get("techTags"):
                addTag(json.dumps({"project_id":str(dictionary.get("id")), "tag":x}, indent = 4))

        if dictionary.get("semesterTag") != None:
            semesters = session.query(semesterProjectTag).filter_by(projectName = dictionary.get("id"))
            for x in list(semesters.all()):
                session.delete(x)
            session.commit()

            addSemesterTag(json.dumps({"project_id":str(dictionary.get("id")), "tag":dictionary.get("semesterTag")}, indent = 4))

        if dictionary.get("groupName") != None and dictionary.get("students"):
            y = {
                "id" : session.query(group).filter_by(projectID = dictionary.get("id")).first().id,
                "groupName" : dictionary.get("groupName"),
                "students":dictionary.get("students")
            }

            editGroup(json.dumps(y, indent = 4))

        session.commit()

def editGroup(jsonDump):
    dictionary = json.loads(jsonDump)

    from sqlalchemy.orm import Session
    with Session(engine) as session:
        students = session.query(student).filter_by(groupID = dictionary.get("id"))

        for x in list(students.all()):
            session.query(student).filter_by(id = x.id).update({student.groupID : None})
        for x in dictionary.get("students"):
            if isinstance(x, str):
                session.query(student).filter_by(pid = x).update({student.groupID : dictionary.get("id")})
            else:
                session.query(student).filter_by(pid = x.get('pid')).update({student.groupID : dictionary.get("id")})

        session.query(group).filter_by(id = dictionary.get("id")).update({group.groupName : dictionary.get("groupName")})

        session.commit()

# t = {
#     "tag": "testtag"
# }

# testtag = json.dumps(t, indent = 4)

# createTag(testtag)

# x = {
#     "projectName": "project2",
#     "description": "this is a sample project",
#     "link": "nah",
#     "students": [{"pid":"tam@vt.edu", "name" : "TAm tim", "group" : "group1"},
#     {"pid":"tom@vt.edu", "name" : "Tom tom", "group" : "group1"}],
#     "groupName" : "group2"
# }

# l = {"pid":"tam@vt.edu", "name" : "TAm tim"}
# m = json.dumps(l, indent = 4)

# o = {"pid":"tom@vt.edu", "name" : "Tom tim"}
# n = json.dumps(o, indent = 4)

# y = json.dumps(x, indent = 4)

# createStudent(m)
# createStudent(n)
# creatProject(y)
