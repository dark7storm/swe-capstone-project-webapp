# How to set up

`python -m venv .`

`pip install -r requirements.txt`

## To run locally

`export dev=1 && gunicorn --config gunicorn_config.py app.wsgi:app`

## To build and deploy

`docker login container.cs.vt.edu` - login into your CS VT gitlab acc

`docker-compose build --pull` - to build the 2 containers

`docker-compose push` - to push the changes to the listed image tags


