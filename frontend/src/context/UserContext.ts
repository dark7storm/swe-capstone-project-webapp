/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import { createContext } from 'react';

import Role from '../enum/Role';
import User from '../interfaces/User';

export type UserContextType = {
  user?: User;
  login: (u: User) => void;
  logout: () => void;
};

// TODO change here
export const UserContext = createContext<UserContextType>({
  user: {
    name: '',
    pid: '',
    role: Role.Student
  },
  login: (u: User) => {},
  logout: () => {}
});
