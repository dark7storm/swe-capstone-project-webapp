import { Button, Flex, Icon } from '@chakra-ui/react';
import { BiLogIn } from 'react-icons/bi';

import { casLogin } from '../services/cas';
import VtHeader from './VtHeader';
import VtHeading from './VtHeading';

function VtGuestNotif() {
  return (
    <Flex flexDirection="column" w="100vw" h="100%">
      <VtHeader />
      <VtHeading>Please login with VT CS CAS to access these features</VtHeading>
      <Button
        justifyContent="center"
        alignSelf="center"
        w={['75%', '50%', '15%']}
        mt="5"
        bg="maroon"
        variant="solid"
        _hover={{ bg: '#a32148' }}
        leftIcon={<Icon as={BiLogIn} />}
        onClick={casLogin}>
        Login
      </Button>
    </Flex>
  );
}
export default VtGuestNotif;
