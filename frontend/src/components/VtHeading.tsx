import { Heading, HeadingProps } from '@chakra-ui/react';

interface Props {
  children: React.ReactNode;
}
function VtHeading({ children, ...props }: Props | HeadingProps) {
  return (
    <Heading
      mt="8"
      as="h1"
      size={['xl', '2xl']}
      px={['4', '8']}
      color="maroon"
      //alignSelf="flex-start"
      textAlign="center"
      {...props}>
      {children}
    </Heading>
  );
}

export default VtHeading;
