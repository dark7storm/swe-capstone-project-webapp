import {
  Button,
  Flex,
  Grid,
  GridItem,
  Heading,
  Icon,
  IconButton,
  Input,
  LinkBox,
  LinkOverlay,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Spacer,
  Text,
  useColorModeValue,
  useDisclosure,
  useToast
} from '@chakra-ui/react';
import axios from 'axios';
import { createContext, useContext, useEffect, useRef, useState } from 'react';
import { useCookies } from 'react-cookie';
import { BiLogIn, BiLogOut } from 'react-icons/bi';
import { FaHome } from 'react-icons/fa';
import { ImHome } from 'react-icons/im';
import { MdArrowDownward, MdArrowUpward } from 'react-icons/md';
import { Link, NavLink, useNavigate } from 'react-router-dom';

import { axiosCredentials, axiosNoCredentials } from '../axios';
import { UserContext } from '../context/UserContext';
import Role from '../enum/Role';
import { adminRoutes, userRoutes } from '../main';
import { casLogin, casLogout } from '../services/cas';

/**
 * Uses Chakra's ui with grid
 *
 * contains the header logic here
 * @returns
 */
function VtHeader() {
  const toast = useToast();
  const bg = useColorModeValue('maroon', 'maroon');
  const color = useColorModeValue('white', 'white');
  // const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useNavigate();
  const [cookies, setCookie, removeCookie] = useCookies(['token']);
  const { user, login, logout } = useContext(UserContext);
  const [hasProject, setHasProject] = useState(false);
  const routes = user?.role === Role.Admin ? adminRoutes : userRoutes;

  // we'll have to do some auth logic here
  async function studentHasProject() {
    if (user) {
      try {
        const { data, status } = await axiosCredentials(cookies.token).get('/studentgroup');

        if (status === 200) {
          return setHasProject(data.group !== null);
        } else {
          return setHasProject(false);
        }

        console.log('error getting current user group');
        return null;
      } catch (ex) {
        toast({
          title: 'Failed to check if user has group',
          status: 'error',
          duration: 2500,
          isClosable: true
        });
        return null;
      }
    } else {
      return setHasProject(false);
    }
  }

  useEffect(() => {
    studentHasProject();
  }, []);

  return (
    <>
      <Grid w="100vw" bg={bg} color={color} gap={4} templateColumns={`repeat(4, 1fr)`} py="4">
        <GridItem colSpan={2} px={['2', '4', '8']} justifySelf="self-start" alignSelf="center">
          <LinkBox>
            <Heading as="h1" size={['sm', 'sm ', 'md', 'lg', 'xl']}>
              <LinkOverlay href="/">CS4704 Project Database</LinkOverlay>
            </Heading>
          </LinkBox>
        </GridItem>
        {/* <GridItem
          colSpan={1}
          px={['0', '4', '8']}
          alignSelf="center"
          justifySelf="self-end"></GridItem> */}
        <GridItem colSpan={2} px={['0', '4', '8']} mt={0} alignSelf="right" justifySelf="right">
          <IconButton
            aria-label="home button"
            icon={<ImHome />}
            bg="maroon"
            size={['sm', 'md', 'lg']}
            _hover={{ bg: 'hokieStone' }}
            _active={{ bg: 'hokieStone' }}
            _focus={{ bg: 'maroon' }}
            onClick={() => navigate('/')}></IconButton>
          <Menu>
            {({ isOpen }) => (
              <>
                <MenuButton
                  bg="maroon"
                  color="white"
                  _hover={{ bg: 'hokieStone' }}
                  _active={{ bg: 'hokieStone' }}
                  isActive={isOpen}
                  as={Button}
                  size={['sm', 'md', 'lg']}
                  rightIcon={isOpen ? <MdArrowUpward /> : <MdArrowDownward />}>
                  {user ? user.pid : 'Guest'}
                </MenuButton>
                <MenuList bg="maroon">
                  {!user && (
                    <MenuItem
                      key="logincas"
                      bg="maroon"
                      icon={<Icon as={BiLogIn} />}
                      _hover={{ bg: 'hokieStone' }}
                      _active={{ bg: 'hokieStone' }}
                      _focus={{ bg: 'maroon' }}
                      onClick={casLogin}>
                      Login with VT CS CAS
                    </MenuItem>
                  )}
                  {user &&
                    routes.map((e) => {
                      if (e.path === '/') return;
                      if (e.path.split('/').length > 2 && e.path.split('/')[1] === 'group') return;
                      if (
                        user.role === Role.Admin &&
                        e.path.split('/').length > 2 &&
                        e.path.split('/')[1] === 'project'
                      )
                        return;
                      if (
                        user.role === Role.Student &&
                        e.path.split('/').length > 2 &&
                        e.path.split('/')[1] === 'project' &&
                        e.path.split('/')[2] === ':id'
                      )
                        return;
                      if (e.path === '/project' && !hasProject) return;
                      // return (
                      //   <MenuItem
                      //     key={e.path}
                      //     bg="maroon"
                      //     _hover={{ bg: 'hokieStone' }}
                      //     _active={{ bg: 'hokieStone' }}
                      //     _focus={{ bg: 'maroon' }}
                      //     onClick={() => navigate('/project')}>
                      //     <Flex flexDirection="row" alignItems="center" gap={2}>
                      //       <Icon as={e.icon} color="white" />
                      //       {e.id}
                      //     </Flex>
                      //   </MenuItem>
                      // );
                      return (
                        <MenuItem
                          key={e.path}
                          bg="maroon"
                          _hover={{ bg: 'hokieStone' }}
                          _active={{ bg: 'hokieStone' }}
                          _focus={{ bg: 'maroon' }}
                          onClick={() => navigate(e.path)}>
                          <Flex flexDirection="row" alignItems="center" gap={2}>
                            <Icon as={e.icon} color="white" />
                            {e.id}
                          </Flex>
                        </MenuItem>
                      );
                    })}
                  {user && (
                    <MenuItem
                      key="logoutcas"
                      icon={<Icon as={BiLogOut} />}
                      bg="maroon"
                      _hover={{ bg: 'hokieStone' }}
                      _active={{ bg: 'hokieStone' }}
                      _focus={{ bg: 'maroon' }}
                      onClick={() => casLogout(removeCookie, navigate)}>
                      Logout
                    </MenuItem>
                  )}
                  {/* <MenuItem
                    onClick={async () => {
                      const res = await axiosNoCredentials().get('/testing', {
                        withCredentials: true
                      });

                      console.log(res);
                    }}>
                    Testing lmao
                  </MenuItem> */}
                </MenuList>
              </>
            )}
          </Menu>
        </GridItem>
      </Grid>

      {/* <Drawer isOpen={isOpen} placement="right" onClose={onClose} finalFocusRef={btnRef}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          {user !== undefined ? (
            <>
              <DrawerHeader>Hello, {user.name}</DrawerHeader>

              <DrawerBody>
                <Input placeholder="Type here..." />
              </DrawerBody>
            </>
          ) : (
            <>
              <DrawerHeader>Login with your VT CS account</DrawerHeader>

              <DrawerBody>
                <Button mr={3} onClick={casLogin} variant="solid" colorScheme="maroon" bg="maroon">
                  VT CAS Login
                </Button>
              </DrawerBody>
            </>
          )}
          <DrawerHeader>Links</DrawerHeader>
          <DrawerBody>
            <Flex flexDirection="column" alignItems="flex-start">
              {routes.map((e) => {
                return (
                  <Button variant="link" colorScheme="maroon" key={e.path} p={2} m={2}>
                    <Link to={e.path}>{e.id}</Link>
                  </Button>
                );
              })}
            </Flex>
          </DrawerBody>
        </DrawerContent>
      </Drawer> */}
    </>
  );
}

export default VtHeader;
