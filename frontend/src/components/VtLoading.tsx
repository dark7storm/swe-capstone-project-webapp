import { Flex } from '@chakra-ui/react';

function VtLoading() {
  return (
    <Flex flexDirection="column" justifyItems="center" alignItems="center">
      Loading...
    </Flex>
  );
}

export default VtLoading;
