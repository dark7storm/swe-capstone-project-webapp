import { Box, Flex } from '@chakra-ui/react';
import ReactLoading from 'react-loading';

import VtHeader from './VtHeader';

function VtSpinner() {
  return (
    <Flex flexDirection="column" alignItems="center" w="100vw" h="100%" justifyContent="center">
      <VtHeader />
      <Box my="4" />
      <ReactLoading color="#861F41" height={'10%'} width={'10%'} type={'spin'} />
    </Flex>
  );
}

export default VtSpinner;
