import {
  Box,
  Button,
  Divider,
  Flex,
  FormLabel,
  IconButton,
  Image,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Popover,
  PopoverAnchor,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  Tag,
  useDisclosure
} from '@chakra-ui/react';
import { MdClose } from 'react-icons/md';
import { useNavigate } from 'react-router-dom';

import { ProjectCard } from '../interfaces/ProjectCards';
import VtHeading from './VtHeading';
import VtText from './VtText';

interface Props {
  project: ProjectCard;
}

// for local dev
// const imageBaseURL = 'http://localhost:3999/image';
const imageBaseURL = 'https://havensprings.discovery.cs.vt.edu/image';
function VtProjectCard({ project }: Props) {
  const navigate = useNavigate();
  const { isOpen, onToggle, onClose } = useDisclosure();

  const { projectName, description, semesterTag, techTags, id, images } = project;

  return (
    <>
      <Box
        // maxW={['xs', 'xs', 'xs', 'sm']}
        // maxH={['xs', 'xs', 'xs', 'sm']}
        borderWidth="1px"
        borderRadius="lg"
        borderColor="maroon"
        m="4"
        _hover={{ backgroundColor: 'maroon', color: 'white' }}
        // onMouseEnter={onToggle}
        // onMouseLeave={() => {
        //   if (isOpen) onToggle();
        // }}
        onClick={onToggle}>
        <Box py="2" px="4">
          {projectName}
        </Box>
        <Divider />
        <Box px="2">
          <Tag m="2">{semesterTag}</Tag>
          {techTags.length > 0 && <Tag m="2">{techTags[0]}</Tag>}
          {techTags.length > 1 && <Tag m="2">{techTags[1]}</Tag>}
          {techTags.length > 2 && <Tag m="2">...</Tag>}
        </Box>
        {images.length > 0 && (
          <>
            <Divider />
            <Flex
              alignItems="center"
              justifyItems="center"
              alignContent="center"
              justifyContent="center"
              width="100%"
              height="100%">
              <Image
                alt={projectName}
                src={`${imageBaseURL}/${images[0]}`}
                borderBottomRadius="lg"
                objectFit="scale-down"
                maxWidth="100%"
                maxHeight="100%"
              />
            </Flex>
          </>
        )}
      </Box>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent
          ringColor="maroon"
          outlineColor="maroon"
          color="maroon"
          borderColor="maroon"
          p="4"
          // onMouseEnter={() => {
          //   onToggle();
          // }}
          // onMouseLeave={() => {
          //   if (isOpen) onToggle();
          // }}
          m="4">
          {/* <ModalHeader>{name}</ModalHeader> */}

          <ModalBody>
            <Box
              maxW={['xs', 'xs', 'xs', 'sm']}
              maxH={['xs', 'xs', 'xs', 'sm']}
              borderWidth="1px"
              borderRadius="lg"
              borderColor="maroon"
              _hover={{ backgroundColor: 'maroon', color: 'white' }}
              // onMouseEnter={onToggle}
              // onMouseLeave={() => {
              //   if (isOpen) onToggle();
              // }}
              mb="2">
              <Box py="2" px="4">
                {projectName}
              </Box>
              <Divider />
              <Box px="2">
                <Tag m="2">{semesterTag}</Tag>
                {techTags.map((e) => (
                  <Tag m="2" key={e}>
                    {e}
                  </Tag>
                ))}
                {/* {techTags.length > 0 && }
                {techTags.length > 1 && <Tag m="2">{techTags[1]}</Tag>}
                {techTags.length > 2 && <Tag m="2">...</Tag>} */}
              </Box>
              {/* <Image
                alt={projectName}
                src={images.length > 0 ? `${imageBaseURL}/${images[0]}` : ''}
                borderBottomRadius="lg"
              /> */}
            </Box>
            <FormLabel mt="4">Description</FormLabel>
            {description}
          </ModalBody>
          <ModalFooter>
            <IconButton
              icon={<MdClose />}
              variant="outline"
              bg="white"
              size="sm"
              outlineColor="maroon"
              aria-label={`close ${name} popup`}
              m="2"
              onClick={onToggle}
            />
            <Button
              variant="solid"
              bg="maroon"
              textColor="white"
              _hover={{ bg: '#a32148' }}
              m="2"
              onClick={() => {
                console.log('we go to this project page');
                navigate(`/project/${id}`);
              }}>
              View Project
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default VtProjectCard;
