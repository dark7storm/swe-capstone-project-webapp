import { Text, TextProps } from '@chakra-ui/react';

interface Props {
  children: React.ReactNode;
}

function VtText({ children, ...props }: Props | TextProps) {
  return (
    <Text
      pt="8"
      as="p"
      size={['sm', 'md', 'lg', 'xl']}
      px={[4, 8]}
      color="maroon"
      alignSelf="center"
      {...props}>
      {children}
    </Text>
  );
}

export default VtText;
