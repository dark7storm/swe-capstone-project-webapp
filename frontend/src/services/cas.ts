import { decodeToken, isExpired, useJwt } from 'react-jwt';
import { NavigateFunction } from 'react-router-dom';
import { CookieSetOptions } from 'universal-cookie';

import User, { UserToken } from './../interfaces/User';

// const testingJwtStudent =
//   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik5hdGUiLCJpYXQiOjE5MDAwLCJjb250ZXh0Ijp7Im5hbWUiOiJOYXRlIiwicGlkIjoiZGFyazdzdG9ybSIsInJvbGUiOiJTdHVkZW50In19.5EVBWt-LaLDGlYHAHl2vyDWmUKiPKRmT9C-wAi2NNAo';
// TODO remove this eventually
export const testingJwtStudent =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik5hdGUiLCJleHAiOjE5MDAwMDAwMDAsImNvbnRleHQiOnsibmFtZSI6Ik5hdGUiLCJwaWQiOiJkYXJrN3N0b3JtIiwicm9sZSI6IlN0dWRlbnQifX0.cOz44iOR-9GrOX4cyqjZ94ouWbNs0hpG7Vvh-7pZGKI';

export const testingJwtCharles =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkNoYXJsZXMiLCJleHAiOjE5MDAwMDAwMDAsImNvbnRleHQiOnsibmFtZSI6IkNoYXJsZXMiLCJwaWQiOiJjaGFybGVzIiwicm9sZSI6IkFkbWluIn19.BnxnUgiflt20G-CP-dUlXHBswif_1ywzFh4WJ_Y567U';

export const casLogin = () => {
  window.location.href = `https://havensprings.discovery.cs.vt.edu/login?next=${encodeURIComponent(
    window.location.href
  )}`;
  // window.location.href = `http://localhost:3999/login?next=${encodeURIComponent(
  //   window.location.href
  // )}`;
};

const resetUrl = () => {
  window.history.replaceState('', '', window.location.href.split('?')[0]);
  // window.location.href = window.location.href.split('?')[0];
};

const localStorageLogin = () => {
  const token = localStorage.getItem('token');
  console.log(token);
  if (token) {
    const user = decodeToken<UserToken>(token);
    const expired = isExpired(token);
    console.log(expired);
    if (expired) {
      return undefined;
    }
    console.log(user?.context);
    return user?.context;
  }

  return undefined;
};

export const verifyLogin = (
  cookies: { token?: any },
  setCookies: (name: 'token', value: any, options?: CookieSetOptions | undefined) => void,
  removeCookie: (name: 'token', options?: CookieSetOptions | undefined) => void
) => {
  const navigationType = window.performance.navigation.type;
  // console.log(window.performance.navigation.type ? 'reloaded' : 'redirected');
  // console.log(document.cookie);
  // console.log(location);
  const location = new URLSearchParams(window.location.search.replace('?', ''));
  // console.log(location);
  // console.log('checking cookies');
  // console.log(cookies);
  // TODO: for testing purposes
  // if (token) localStorage.setItem('token', token);
  // else localStorage.setItem('token', testingJwtStudent);
  // console.log(localStorage.getItem('token'));

  console.log('did you reach here');
  console.log(window.performance.navigation.type);

  if (navigationType === 0) {
    // if redirected, need to check if token present or not
    // check local storage, if present, then return the user
    const localStored = decodeToken<UserToken>(cookies.token);
    if (localStored && !isExpired(cookies.token)) {
      resetUrl();
      return localStored?.context;
    } else if (localStored && isExpired(cookies.token)) {
      resetUrl();
      removeCookie('token');
      return undefined;
    }

    // get token, in whichever way we get the token from backend
    const urlToken = location.get('token');
    if (urlToken) {
      const user = decodeToken<UserToken>(urlToken);
      const expired = isExpired(testingJwtStudent);
      if (expired) {
        resetUrl();
        removeCookie('token');
        return undefined;
      }
      setCookies('token', urlToken, { sameSite: false });
      resetUrl();
      return user?.context;
    }
  }

  // check to see if token in local storage
  // const token: string | null = localStorage.getItem('token');
  // right now we use the below for now
  // const token = testingJwtCharles;
  const localStored = decodeToken<UserToken>(cookies.token);
  resetUrl();
  if (localStored && !isExpired(cookies.token)) return localStored?.context;
  else if (localStored && isExpired(cookies.token)) {
    removeCookie('token');
    // casLogin();
  }
  return undefined;

  // if (token) {
  //   const user = decodeToken<UserToken>(token);
  //   const expired = isExpired(token);

  //   if (!user) return undefined;

  //   if (expired) {
  //     // grab new token from server
  //     // get user data
  //     // return user value
  //   }
  //   return user.context;
  // }

  // // if not
  // return undefined;
};

export const casLogout = (
  removeCookie: (name: 'token', options?: CookieSetOptions | undefined) => void,
  navigate: NavigateFunction
) => {
  removeCookie('token');

  navigate('/');
  location.reload();

  // localStorage.removeItem('token');
};

export const casVerifyLogin = (
  cookies: { token?: any },
  setCookies: (name: 'token', value: any, options?: CookieSetOptions | undefined) => void,
  removeCookie: (name: 'token', options?: CookieSetOptions | undefined) => void,
  login: (u: User) => void
) => {
  const res = verifyLogin(cookies, setCookies, removeCookie);

  if (res) {
    console.log('Checking cookies');
    console.log(res);
    login(res);
  }
};
