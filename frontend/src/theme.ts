// 1. import `extendTheme` function
import { extendTheme } from '@chakra-ui/react';

// 2. Add your color mode config
const config = {
  initialColorMode: 'light',
  useSystemColorMode: false
};

// 3. extend the theme
const theme = extendTheme({
  config,
  colors: {
    transparent: 'transparent',
    maroon: '#861F41',
    white: '#E5E1E6',
    black: '#000000',
    hokieStone: '#75787b'
  },
  component: {
    // Button: {
    //   _hover: {
    //     bg: 'hokieStone'
    //   }
    // },
    // MenuItem: {
    //   _hover: {
    //     bg: 'hokieStone'
    //   }
    // },
    // MenuButton: {
    //   variants: {
    //     _hover: {
    //       bg: 'hokieStone'
    //     }
    //   },
    //   defaultProps: {
    //     variant: 'primary'
    //   }
    // }
  }
});

export default theme;
