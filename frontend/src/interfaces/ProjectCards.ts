export interface ProjectCard {
  projectName: string;
  images: string[];
  techTags: string[];
  semesterTag: string;
  description: string;
  id: string;
}
