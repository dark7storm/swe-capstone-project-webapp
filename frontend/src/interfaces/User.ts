import Role from '../enum/Role';

interface User {
  name: string;
  pid: string;
  role: Role;
}

export interface UserToken {
  name: string;
  exp: number;
  sub: number;
  context: User;
}

export default User;
