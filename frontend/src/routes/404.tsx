import { Flex } from '@chakra-ui/react';
import { useRouteError } from 'react-router-dom';

// import VtHeader from '../components/VtHeader';
import VtHeading from '../components/VtHeading';

export default function Error() {
  const error = useRouteError();
  console.error(error);
  return (
    <Flex flexDirection="column" w="100vw" h="100%">
      {/* <VtHeader /> */}
      <VtHeading>An error occurred in loading this page (404).</VtHeading>
    </Flex>
  );
}
