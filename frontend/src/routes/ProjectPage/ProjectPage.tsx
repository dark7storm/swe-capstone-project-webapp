import './ProjectPage.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { Card, CardBody, CardHeader } from '@chakra-ui/card';
import { DeleteIcon, EditIcon, ExternalLinkIcon } from '@chakra-ui/icons';
import {
  Box,
  Button,
  Center,
  Checkbox,
  Flex,
  Grid,
  GridItem,
  Heading,
  Icon,
  Image,
  Link,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  StackDivider,
  Tag,
  TagLabel,
  TagLeftIcon,
  Text,
  Textarea,
  Tooltip,
  useDisclosure,
  useToast
} from '@chakra-ui/react';
import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { BiLogIn } from 'react-icons/bi';
import { BsPersonFill } from 'react-icons/bs';
import { Form, useNavigate, useParams, useSearchParams } from 'react-router-dom';
import Slider from 'react-slick';

import { axiosCredentials, axiosNoCredentials } from '../../axios';
import VtHeader from '../../components/VtHeader';
import VtHeading from '../../components/VtHeading';
import VtLoading from '../../components/VtLoading';
import YoutubeEmbed from '../../components/YoutubeEmbed';
import { UserContext } from '../../context/UserContext';
import Role from '../../enum/Role';
import { casLogin, casVerifyLogin } from '../../services/cas';
import wafflehouse from './wafflehouse.jfif';
import wafflehousesign from './wafflehousesign.jpg';

interface Project {
  projectName: string;
  link: string;
  groupName: string;
  students: any[];
  description: string;
  techTags: any[];
  semesterTag: string;
  images: any[];
  video: string;
  comments: any[];
  notes: any[];
}

interface Values {
  newComment: any;
  newNote: any;
  toDeleteComments: any[];
  toDeleteNotes: any[];
}

// const imageBaseURL = 'http://localhost:3999/image';
const imageBaseURL = 'https://havensprings.discovery.cs.vt.edu/image';

function ProjectPage() {
  const toast = useToast();
  const { id } = useParams();
  const navigate = useNavigate();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [cookies, setCookie, removeCookie] = useCookies(['token']);
  const { user, login } = useContext(UserContext);
  const [identity, setIdentity] = useState('');
  const [loading, setLoading] = useState(true);
  const [projectId, setProjectId] = useState(id);
  const [values, setValues] = useState<Values>({
    newComment: {},
    newNote: {},
    toDeleteComments: [],
    toDeleteNotes: []
  });
  const [project, setProject] = useState<Project>({
    projectName: '',
    link: '',
    groupName: '',
    students: [],
    description: '',
    techTags: [],
    semesterTag: '',
    images: [],
    video: '',
    comments: [],
    notes: []
  });

  useEffect(() => {
    console.log('identity' + identity);
  }, [identity]);

  useEffect(() => {
    console.log('getting project...');
    // if no id, use usercontext to grab their project
    if (user?.role === Role.Admin) {
      setIdentity('admin');
    } else if (user?.role === Role.Student) {
      // check if currentUserGroup matches id (project id)
      getStudentIdentity();
    }

    if (projectId) {
      getProject(projectId);
    }

    setLoading(false);
  }, [user]);

  useEffect(() => {
    casVerifyLogin(cookies, setCookie, removeCookie, login);

    console.log(id);
    console.log('ROLE');
    console.log(user?.role);

    // admin

    // student (it's their project)
    // else if (user?.role === Role.Student && user?.group?.id === projectId) {
    //   setIdentity('author');
    // }
    // // student (it's not their project)
    // else if (user?.role === Role.Student && user?.group?.id !== projectId) {
    //   setIdentity('viewer');
    // }

    // setProject({
    //   projectName: '',
    //   link: '',
    //   groupName: '',
    //   students: [],
    //   description: '',
    //   techTags: [],
    //   semesterTag: '',
    //   images: [],
    //   video: '',
    //   comments: [],
    //   notes: []
    // });
  }, []);

  // Get current user group
  const getCurrentUserGroup = async () => {
    try {
      const { data, status } = await axiosCredentials(cookies.token).get('/studentgroup');

      if (status === 200) {
        return data;
      }

      console.log('Error getting current user group');
      return null;
    } catch (error) {
      toast({
        title: 'Failed to check if user has group or not',
        status: 'error',
        duration: 2500,
        isClosable: true
      });
      return null;
    }
  };

  async function getStudentIdentity() {
    console.log('Student identity function');
    const data = await getCurrentUserGroup();
    console.log(id);
    console.log(projectId);

    if (!projectId && data.group !== null) {
      console.log('clicked the dropdown Project button');
      setProjectId(data.group.id.toString());
      getProject(data.group.id.toString());
      setIdentity('author');
    } else if (data.group !== null && data.group.id.toString() === projectId) {
      console.log('Student has group and is in this one');
      setIdentity('author');
    } else {
      console.log('Student does not have group OR is in a different group');
      setIdentity('viewer');
    }
  }

  // // Get comments
  // const getComments = async (id: any) => {
  //   const commentsFailedToLoad = () => {
  //     toast({
  //       title: 'Comments failed to load',
  //       status: 'error',
  //       duration: 2500,
  //       isClosable: true
  //     });

  //     setProjectComments([]);
  //   };

  //   try {
  //     console.log('before getting comments');
  //     const { data, status } = await axiosCredentials(cookies.token).get(
  //       '/projects/' + id + '/comments'
  //     );
  //     console.log(data);
  //     if (status === 200) {
  //       setProjectComments(data.comments);

  //       //setLoading(false);
  //       return;
  //     }

  //     commentsFailedToLoad();
  //   } catch (e) {
  //     commentsFailedToLoad();
  //   }
  // };

  // // Get notes
  // const getNotes = async (id: any) => {
  //   const notesFailedToLoad = () => {
  //     toast({
  //       title: 'Notes failed to load.',
  //       status: 'error',
  //       duration: 2500,
  //       isClosable: true
  //     });

  //     setProjectNotes([]);
  //   };

  //   try {
  //     console.log('before get notes');
  //     const { data, status } = await axiosCredentials(cookies.token).get(
  //       '/getNotesForProject/' + id
  //     );
  //     console.log(data);

  //     if (status === 200) {
  //       setProjectNotes(data.notes);

  //       //setLoading(false);
  //       return;
  //     }

  //     notesFailedToLoad();
  //   } catch (e) {
  //     notesFailedToLoad();
  //   }
  // };

  // Get project
  const getProject = async (id: any) => {
    const projectFailedToLoad = () => {
      toast({
        title: 'Project failed to load',
        status: 'error',
        duration: 2500,
        isClosable: true
      });

      setProject({
        projectName: '',
        link: '',
        groupName: '',
        students: [],
        description: '',
        techTags: [],
        semesterTag: '',
        images: [],
        video: '',
        comments: [],
        notes: []
      });
      setLoading(false);
    };

    // console.log('waiting for promise');
    // await Promise.all([getComments(id), getNotes(id)]);
    // console.log('promise');
    // console.log(projectComments);
    // console.log(projectNotes);

    try {
      let newProject;
      console.log('Id:');
      console.log(id);
      const { data, status } = await axiosNoCredentials().get('/getprojectwithid/' + id);
      if (status === 200) {
        console.log(data);
        newProject = {
          projectName: data.projectName,
          link: data.link,
          groupName: data.groupName,
          students: data.students,
          description: data.description,
          techTags: data.techTags,
          semesterTag: data.semester,
          images: data.images,
          video: getEmbedId(data.video),
          comments: [],
          notes: []
          // comments and notes have separate getters
        };

        // get comments
        const { data: commentsList } = await axiosCredentials(cookies.token).get(
          '/projects/' + id + '/comments'
        );
        newProject = { ...newProject, comments: commentsList.comments };

        // get notes
        const { data: notesList } = await axiosCredentials(cookies.token).get(
          '/getNotesForProject/' + id
        );
        newProject = { ...newProject, notes: notesList.notes };

        setProject(newProject);
        setLoading(false);
        return;
        // try {
        //   const { data, status } = await axiosNoCredentials().get('/projects/' + id + '/comments');

        //   if (status === 200) {
        //     newProject = { ...newProject, comments: data.comments };
        //     try {
        //       // GET notes from backend
        //       console.log('before getting notes');
        //       console.log(id);
        //       const { data, status } = await axiosCredentials(cookies.token).get(
        //         '/projects/' + id + '/notes'
        //       );
        //       console.log('after getting notes');

        //       if (status === 200) {
        //         console.log('got notes');
        //         console.log(data);
        //         newProject = { ...newProject, notes: data.notes };

        //         setProject(newProject);

        //         setLoading(false);
        //         return;
        //       }
        //     } catch (e) {
        //       console.log(e);
        //       notesFailedToLoad();
        //     }
        //   } else {
        //     commentsFailedToLoad();
        //   }
        // } catch (e) {
        //   commentsFailedToLoad();
        // }
        // setLoading(false);
        // return;
      }
      projectFailedToLoad();
    } catch (e) {
      projectFailedToLoad();
    }
  };

  /**
   * Helper function. Given a youtube url, returns the videoId
   *
   * @param link the youtube link
   *
   * @returns the videoId
   */
  function getEmbedId(link: string): string {
    // Our regex pattern to look for a youTube ID
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    //Match the url with the regex
    const match = link.match(regExp);
    //Return the result
    return match && match[2].length === 11 ? match[2] : '';
  }

  // guest
  if (!user)
    return (
      <Flex flexDirection="column" w="100vw" h="100%">
        <VtHeader />
        <VtHeading>Please login with VT CS CAS to access these features</VtHeading>
        <Button
          justifyContent="center"
          alignSelf="center"
          w={['75%', '50%', '15%']}
          mt="5"
          bg="maroon"
          variant="solid"
          _hover={{ bg: '#a32148' }}
          leftIcon={<Icon as={BiLogIn} />}
          onClick={casLogin}>
          Login
        </Button>
      </Flex>
    );

  if (loading) return <VtLoading />;

  const fieldWidth = ['85%', '60%', '50%'];

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
    adaptiveHeight: true
  };

  // TODO: delete this
  let imageKey = 1;

  return (
    <Flex flexDirection="column" w="100vw" h="100%" alignItems="center" justifyContent="center">
      <VtHeader />
      <Card w={fieldWidth}>
        <CardHeader alignSelf="center" paddingBottom={4}>
          <VtHeading>{project.projectName}</VtHeading>
          {(identity === 'admin' || identity === 'author') && (
            <Grid templateColumns="repeat(5, 1fr)">
              <GridItem colSpan={1}>
                <Button
                  mt="3"
                  leftIcon={<EditIcon />}
                  size="md"
                  variant="solid"
                  bg="green.700"
                  textColor="white"
                  onClick={() => {
                    navigate(`/submission?id=${projectId}`);
                  }}>
                  Edit
                </Button>
              </GridItem>
              <GridItem colSpan={3} />
              <GridItem colSpan={1}>
                <Button
                  mt="3"
                  leftIcon={<DeleteIcon />}
                  size="md"
                  variant="solid"
                  colorScheme="maroon"
                  bg="maroon"
                  _hover={{ bg: '#a32148' }}
                  onClick={onOpen}>
                  Delete
                </Button>
                <Modal isOpen={isOpen} onClose={onClose}>
                  <ModalOverlay />
                  <ModalContent>
                    <ModalHeader>Delete Project</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                      Delete project {project.projectName}? This action cannot be reversed.
                    </ModalBody>
                    <ModalFooter>
                      <Flex width="100%" flexDirection="row">
                        <Button flex="1" alignSelf="flex-start" colorScheme="red" onClick={onClose}>
                          Cancel
                        </Button>
                        <Box flex="1" />
                        <Box flex="1" />
                        <Box flex="1" />
                        <Button
                          flex="1"
                          alignSelf="flex-end"
                          variant="solid"
                          colorScheme="maroon"
                          bg="maroon"
                          leftIcon={<DeleteIcon />}
                          onClick={async (e) => {
                            console.log('Delete project function starts');
                            try {
                              const { status } = await axiosCredentials(cookies.token).delete(
                                '/deleteproject/' + projectId
                              );

                              if (status === 200) {
                                toast({
                                  title: `Project ${project.projectName} was deleted successfully.`,
                                  status: 'success',
                                  duration: 2500,
                                  isClosable: true
                                });
                                navigate('/');
                              }
                            } catch (ex) {
                              toast({
                                title: 'Project was not deleted, there was an error.',
                                status: 'error',
                                duration: 2500,
                                isClosable: true
                              });
                            }
                          }}>
                          Delete
                        </Button>
                      </Flex>
                    </ModalFooter>
                  </ModalContent>
                </Modal>
              </GridItem>
            </Grid>
          )}
        </CardHeader>
        <CardBody>
          <Stack spacing="4">
            <Box>
              <Heading size={['xs', 'sm']} color="black">
                Group Name
              </Heading>
              <Text pt="1" size="xs" color="black">
                {project.groupName}
              </Text>
            </Box>
            {(identity === 'admin' || identity === 'author') && (
              <Box>
                <Heading size={['xs', 'sm']} color="black" paddingBottom="2">
                  Group Members
                </Heading>
                {project.students.map((student) => (
                  <Tooltip
                    hasArrow
                    label={student.name}
                    key={student.pid}
                    bg="maroon"
                    placement="top">
                    <Tag
                      mr="2"
                      mb="1"
                      size="md"
                      key={student.pid}
                      variant="subtle"
                      colorScheme="red">
                      <TagLeftIcon as={BsPersonFill}></TagLeftIcon>
                      <TagLabel>{student.pid}</TagLabel>
                    </Tag>
                  </Tooltip>
                ))}
              </Box>
            )}
            <Box>
              <Heading size={['xs', 'sm']} color="black" paddingBottom="2">
                Tech Stack Tags
              </Heading>
              {project.techTags.map((tag) => (
                <Tag mr="2" size="md" key={tag} variant="subtle" colorScheme="red">
                  {tag}
                </Tag>
              ))}
            </Box>
            <Box>
              <Heading size={['xs', 'sm']} color="black">
                Semester
              </Heading>
              <Text pt="1" size="xs" color="black">
                {project.semesterTag}
              </Text>
            </Box>
            {(identity === 'admin' || identity === 'author') && (
              <Box>
                <Heading size={['xs', 'sm']} color="black">
                  Repository
                </Heading>
                <Link pt="1" href={project.link} isExternal textColor="blue">
                  {project.link} <ExternalLinkIcon mx="2px" />
                </Link>
              </Box>
            )}
            <Box>
              <Heading size={['xs', 'sm']} color="black">
                Description
              </Heading>
              <Text pt="1" size="xs" color="black">
                {project.description}
              </Text>
            </Box>
            {project.images.length > 0 && (
              <Box>
                <Heading pb="2" size={['xs', 'sm']} color="black">
                  Images
                </Heading>
                <Slider {...settings}>
                  {project.images.map((image) => (
                    <div key={imageKey++}>
                      <Center>
                        <Image src={`${imageBaseURL}/${image}`} />
                      </Center>
                    </div>
                  ))}
                </Slider>
              </Box>
            )}
            {project.video.length > 0 && (
              <Box>
                <Heading pb="1" size={['xs', 'sm']} color="black">
                  Video
                </Heading>
                <YoutubeEmbed embedId={project.video} />
              </Box>
            )}
            <Box>
              <Heading pb="1" size={['xs', 'sm']} color="black">
                Comments
              </Heading>
              <Form>
                <Textarea
                  placeholder="Add a commment..."
                  size="sm"
                  focusBorderColor="maroon"
                  borderColor="black"
                  textColor="black"
                  variant="filled"
                  value={values.newComment.text}
                  onChange={(e) =>
                    setValues((prev) => {
                      return {
                        ...prev,
                        newComment: {
                          text: e.target.value,
                          pid: user?.pid,
                          projectID: projectId
                        }
                      };
                    })
                  }
                />
                <Button
                  size="sm"
                  variant="solid"
                  colorScheme="maroon"
                  bg="maroon"
                  type="submit"
                  my="2"
                  onClick={async (e) => {
                    // post newComment to database
                    if (values.newComment.text?.length === 0) {
                      toast({
                        title: 'No empty comments allowed',
                        description: 'Please enter a valid string in the input field',
                        status: 'error',
                        duration: 2500,
                        isClosable: true
                      });
                      setValues((prev) => {
                        return {
                          ...prev,
                          newComment: {
                            text: '',
                            pid: user?.pid,
                            projectID: projectId
                          }
                        };
                      });
                      return;
                    }

                    try {
                      console.log(values.newComment);
                      console.log(projectId);
                      const { data, status } = await axiosNoCredentials().post(
                        '/makecomment',
                        values.newComment
                      );

                      if (status === 200) {
                        toast({
                          title: 'Added comment',
                          status: 'success',
                          duration: 2500,
                          isClosable: true
                        });
                        setProject({
                          ...project,
                          comments: [...project.comments, { ...values.newComment, id: data.id }]
                        });
                        setValues((prev) => {
                          return {
                            ...prev,
                            newComment: {
                              text: '',
                              pid: user?.pid,
                              projectID: projectId
                            }
                          };
                        });
                        return;
                      }
                      toast({
                        title: 'Error: could not add comment',
                        status: 'error',
                        duration: 2500,
                        isClosable: true
                      });
                    } catch (ex) {
                      toast({
                        title: 'Error: could not add comment',
                        status: 'error',
                        duration: 2500,
                        isClosable: true
                      });
                    }
                  }}>
                  Add Comment
                </Button>
              </Form>
              {identity === 'admin' && (
                <Form>
                  <Stack spacing="1">
                    {project.comments?.map((comment) => (
                      <Box key={comment.id} border="1px" borderColor="maroon" borderRadius="md">
                        <Checkbox
                          colorScheme="red"
                          border="maroon"
                          padding="2"
                          onChange={(t) => {
                            if (t.target.checked) {
                              console.log(comment);
                              setValues((p) => {
                                console.log(p.toDeleteComments);
                                p.toDeleteComments.push(comment.id);
                                console.log(p.toDeleteComments);
                                return { ...p };
                              });
                              return;
                            }

                            setValues((p) => {
                              p.toDeleteComments = p.toDeleteComments.filter(
                                (f) => f !== comment.id
                              );
                              return { ...p };
                            });
                          }}>
                          <Heading size="xs">{comment.pid}</Heading>
                          <Text pt="1" fontSize="sm">
                            {comment.text}
                          </Text>
                        </Checkbox>
                      </Box>
                    ))}
                  </Stack>
                  {project.comments?.length > 0 && (
                    <Button
                      size="sm"
                      variant="solid"
                      colorScheme="maroon"
                      bg="maroon"
                      type="submit"
                      my="2"
                      onClick={async (e) => {
                        // DELETE from database
                        console.log(values.toDeleteComments);
                        if (values.toDeleteComments.length === 0) {
                          toast({
                            title: 'No comments were selected!',
                            description: 'Please select the comment(s) to remove',
                            status: 'error',
                            duration: 2500,
                            isClosable: true
                          });
                          return;
                        }
                        try {
                          console.log(values.toDeleteComments);
                          const { data, status } = await axiosNoCredentials().delete('/comments', {
                            data: {
                              ids: values.toDeleteComments
                            }
                          });

                          if (status === 200) {
                            toast({
                              title: 'Selected comments were deleted',
                              status: 'success',
                              duration: 2500,
                              isClosable: true
                            });
                            console.log(data);
                            setProject({ ...project, comments: data.comments });
                            setValues({ ...values, toDeleteComments: [] });
                          }
                        } catch (ex) {
                          toast({
                            title: 'Comments were not deleted!',
                            description:
                              'There was a server-side error, or you do not have permission to do this.',
                            status: 'error',
                            duration: 2500,
                            isClosable: true
                          });
                        }
                        //   const newProject = project;
                        //   values.toDeleteComments.forEach((entry) => {
                        //     newProject.comments = newProject.comments.filter(
                        //       (comment) => comment.id !== entry
                        //     );
                        //   });
                        //   setProject(JSON.parse(JSON.stringify(newProject)));
                        //   values.toDeleteComments = [];
                        //   setValues(JSON.parse(JSON.stringify(values)));
                      }}>
                      Delete Selected Comment(s)
                    </Button>
                  )}
                </Form>
              )}
              {(identity === 'author' || identity === 'viewer') && (
                <Card>
                  <CardBody>
                    <Stack divider={<StackDivider color="maroon" />} spacing="1">
                      {project.comments.map((comment) => (
                        <Box border="1px" borderColor="maroon" borderRadius="md" key={comment.id}>
                          <Box padding="2">
                            <Heading size="xs">{comment.pid}</Heading>
                            <Text pt="1" fontSize="sm">
                              {comment.text}
                            </Text>
                          </Box>
                        </Box>
                      ))}
                    </Stack>
                  </CardBody>
                </Card>
              )}
            </Box>
            {/* NOTES STARTS HERE */}
            {identity === 'admin' && (
              <Box>
                <Heading pb="1" size={['xs', 'sm']} color="black">
                  Notes
                </Heading>
                <Form>
                  <Textarea
                    placeholder="Add a note..."
                    size="sm"
                    focusBorderColor="maroon"
                    borderColor="black"
                    textColor="black"
                    variant="filled"
                    value={values.newNote.text}
                    onChange={(e) =>
                      setValues((prev) => {
                        return {
                          ...prev,
                          newNote: {
                            text: e.target.value,
                            projectID: projectId
                          }
                        };
                      })
                    }
                  />
                  <Button
                    size="sm"
                    variant="solid"
                    colorScheme="maroon"
                    bg="maroon"
                    type="submit"
                    my="2"
                    onClick={async (e) => {
                      // post newNote to database
                      if (values.newNote.text?.length === 0) {
                        toast({
                          title: 'No empty notes allowed',
                          description: 'Please enter a valid string in the input field',
                          status: 'error',
                          duration: 2500,
                          isClosable: true
                        });
                        setValues((prev) => {
                          return {
                            ...prev,
                            newNote: {
                              text: '',
                              projectID: projectId
                            }
                          };
                        });
                        return;
                      }

                      try {
                        console.log(values.newNote);
                        console.log(projectId);
                        const { data, status } = await axiosNoCredentials().post(
                          '/notes', // TODO: makenote
                          values.newNote
                        );

                        if (status === 200) {
                          toast({
                            title: 'Added note',
                            status: 'success',
                            duration: 2500,
                            isClosable: true
                          });
                          setProject({
                            ...project,
                            notes: [...project.notes, { ...values.newNote, id: data.id }]
                          });
                          setValues((prev) => {
                            return {
                              ...prev,
                              newNote: {
                                text: '',
                                projectID: projectId
                              }
                            };
                          });
                          return;
                        }
                        toast({
                          title: 'Error: could not add note',
                          status: 'error',
                          duration: 2500,
                          isClosable: true
                        });
                      } catch (ex) {
                        toast({
                          title: 'Error: could not add note',
                          status: 'error',
                          duration: 2500,
                          isClosable: true
                        });
                      }
                    }}>
                    Add Note
                  </Button>
                </Form>

                <Form>
                  <Stack spacing="1">
                    {project.notes?.map((note) => (
                      <Box key={note.id} border="1px" borderColor="maroon" borderRadius="md">
                        <Checkbox
                          colorScheme="red"
                          border="maroon"
                          padding="2"
                          onChange={(t) => {
                            if (t.target.checked) {
                              console.log(note);
                              setValues((p) => {
                                console.log(p.toDeleteNotes);
                                p.toDeleteNotes.push(note.id);
                                console.log(p.toDeleteNotes);
                                return { ...p };
                              });
                              return;
                            }

                            setValues((p) => {
                              p.toDeleteNotes = p.toDeleteNotes.filter((f) => f !== note.id);
                              return { ...p };
                            });
                          }}>
                          {/* <Heading size="xs">{note.pid}</Heading> */}
                          <Text fontSize="sm">{note.text}</Text>
                        </Checkbox>
                      </Box>
                    ))}
                  </Stack>
                  {project.notes?.length > 0 && (
                    <Button
                      size="sm"
                      variant="solid"
                      colorScheme="maroon"
                      bg="maroon"
                      type="submit"
                      my="2"
                      onClick={async (e) => {
                        // DELETE from database
                        console.log(values.toDeleteNotes);
                        if (values.toDeleteNotes.length === 0) {
                          toast({
                            title: 'No notes were selected!',
                            description: 'Please select the note(s) to remove',
                            status: 'error',
                            duration: 2500,
                            isClosable: true
                          });
                          return;
                        }
                        try {
                          console.log(values.toDeleteNotes);
                          // TODO: deletenotes endpoint
                          const { data, status } = await axiosNoCredentials().delete('/notes', {
                            data: {
                              ids: values.toDeleteNotes
                            }
                          });

                          if (status === 200) {
                            toast({
                              title: 'Selected notes were deleted',
                              status: 'success',
                              duration: 2500,
                              isClosable: true
                            });
                            console.log(data);
                            setProject({ ...project, notes: data.notes });
                            setValues({ ...values, toDeleteNotes: [] });
                          }
                        } catch (ex) {
                          toast({
                            title: 'Notes were not deleted!',
                            description:
                              'There was a server-side error, or you do not have permission to do this.',
                            status: 'error',
                            duration: 2500,
                            isClosable: true
                          });
                        }
                        //   const newProject = project;
                        //   values.toDeleteComments.forEach((entry) => {
                        //     newProject.comments = newProject.comments.filter(
                        //       (comment) => comment.id !== entry
                        //     );
                        //   });
                        //   setProject(JSON.parse(JSON.stringify(newProject)));
                        //   values.toDeleteComments = [];
                        //   setValues(JSON.parse(JSON.stringify(values)));
                      }}>
                      Delete Selected Note(s)
                    </Button>
                  )}
                </Form>
              </Box>
            )}
          </Stack>
        </CardBody>
      </Card>
    </Flex>
  );
}

export default ProjectPage;
