import {
  Button,
  Container,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Icon,
  IconButton,
  Input,
  LightMode,
  Text,
  Textarea,
  useColorMode,
  useTheme
} from '@chakra-ui/react';
import { useToast } from '@chakra-ui/react';
import { create } from 'lodash';
import { SetStateAction, useContext, useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { BiLogIn } from 'react-icons/bi';
import { BsConeStriped } from 'react-icons/bs';
import { MdCancel } from 'react-icons/md';
import { Form, useNavigate, useSearchParams } from 'react-router-dom';
import Select from 'react-select';

import { axiosCredentials, axiosNoCredentials } from '../../axios';
import VtHeader from '../../components/VtHeader';
import VtHeading from '../../components/VtHeading';
import VtLoading from '../../components/VtLoading';
import VtSpinner from '../../components/VtSpinner';
import VtText from '../../components/VtText';
import { UserContext } from '../../context/UserContext';
import Role from '../../enum/Role';
import { casLogin, casVerifyLogin } from '../../services/cas';

// interface ExistingFile {
//   name: string;
// }
interface FormValues {
  id: string;
  projectName: string;
  groupName: string;
  students: string[];
  link: string;
  description: string;
  techTags: string[];
  semesterTag: string;
  images: File[];
  videos: string;
}

interface AllTagsProps {
  techTags: [];
  semesterTags: [];
}

function SubmissionPageStudent() {
  const toast = useToast();
  const [cookies, setCookie, removeCookie] = useCookies(['token']);

  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const { user, login } = useContext(UserContext);
  const [loading, setLoading] = useState(true);
  const [hasBefore, setHasBefore] = useState(false);
  const [values, setValues] = useState<FormValues>({
    id: '',
    projectName: '',
    link: '',
    groupName: '',
    students: [],
    description: '',
    techTags: [],
    semesterTag: '',
    images: [],
    videos: ''
  });
  const [availableTechTags, setAvailableTechTags] = useState<any[]>([]);
  const [availableSemesterTags, setAvailableSemesterTags] = useState<any[]>([]);
  const [availableStudents, setAvailableStudents] = useState<any[]>([]);
  const [defaultSemesterTag, setDefaultSemesterTag] = useState<{
    [key: string]: any;
  }>({});
  const [defaultTechTags, setDefaultTechTags] = useState<any[]>([]);
  const [defaultStudents, setDefaultStudents] = useState<any[]>([]);

  // const [currentUserGroup, setCurrentUserGroup] = useState({});

  const imageLimit = 6;
  const videoLimit = 2;

  const fieldWidth = '50%';

  // let defaultSemesterTag = ;
  // let defaultTechTags = values.techTags.map((e) => {
  //   return { value: e, label: e };
  // });

  // Get list of available students
  const getAllStudents = async (id = null) => {
    const studentsFailedtoLoad = () => {
      toast({
        title: 'Students failed to load',
        status: 'error',
        duration: 2500,
        isClosable: true
      });

      setAvailableStudents([]);
      setLoading(false);
    };

    try {
      const { data, status } = await axiosNoCredentials().get(
        `/allstudents${id ? '?id=' + id : ''}`
      );
      if (status === 200) {
        console.log(data);

        const studentsList: SetStateAction<any[]> = [];
        data.students.forEach((element: { pid: any }) => {
          studentsList.push({
            label: element.pid,
            value: element.pid
          });
        });
        setAvailableStudents(studentsList);
        setLoading(false);
        return;
      }
      studentsFailedtoLoad();
    } catch (e) {
      studentsFailedtoLoad();
    }
  };

  // Get list of available tech tags and semester tags
  const getAllTags = async () => {
    const tagsFailedToLoad = () => {
      toast({
        title: 'Tags failed to load',
        status: 'error',
        duration: 2500,
        isClosable: true
      });

      setAvailableSemesterTags([]);
      setAvailableTechTags([]);

      setLoading(false);
    };

    try {
      const { data, status } = await axiosNoCredentials().get<AllTagsProps>('/alltags');
      if (status === 200) {
        const techTags: SetStateAction<any[]> = [];
        const semesterTags: SetStateAction<any[]> = [];
        data.techTags.forEach((element) => {
          techTags.push({
            label: element,
            value: element
          });
        });
        data.semesterTags.forEach((element) => {
          semesterTags.push({
            label: element,
            value: element
          });
        });

        setAvailableTechTags(techTags);
        setAvailableSemesterTags(semesterTags);

        setLoading(false);
        return;
      }
      tagsFailedToLoad();
    } catch (e) {
      tagsFailedToLoad();
    }
  };
  const projectFailedToLoad = () => {
    toast({
      title: 'Project failed to load',
      status: 'error',
      duration: 2500,
      isClosable: true
    });

    setValues({
      id: '',
      projectName: '',
      link: '',
      groupName: '',
      students: [],
      description: '',
      techTags: [],
      semesterTag: '',
      images: [],
      videos: ''
    });
    setLoading(false);
  };

  const getProject = async (id: string) => {
    try {
      const { data, status } = await axiosNoCredentials().get('getprojectwithid/' + id);
      if (status === 200) {
        console.log(data);
        setValues((prev) => {
          setDefaultSemesterTag({ value: data.semester, label: data.semester });
          setDefaultTechTags(data.techTags.map((e: string) => ({ value: e, label: e })));
          setDefaultStudents(data.students.map((e: any) => ({ value: e.pid, label: e.pid })));
          return {
            id: data.id,
            projectName: data.projectName,
            link: data.link,
            groupName: data.groupName,
            students: data.students,
            description: data.description,
            techTags: data.techTags,
            semesterTag: data.semester,
            images: [],
            videos: data.video
          };
        });

        const { data: imagelist } = await axiosNoCredentials().get('/imagelist?id=' + id);
        setValues((p) => {
          const { images } = imagelist;
          return { ...p, images: images.map((e: any) => new File([], e.name)) };
        });

        setLoading(false);
        setHasBefore(true);
        return;
      }
      projectFailedToLoad();
    } catch (e) {
      projectFailedToLoad();
    }
  };

  const isAuthorOf = async () => {
    let isAuthor = false;
    values.students.forEach((student: any) => {
      if (student.pid === user?.pid) {
        isAuthor = true;
      }
    });

    return isAuthor;
  };

  const getCurrentUserGroup = async () => {
    try {
      const { data, status } = await axiosCredentials(cookies.token).get('/studentgroup');

      if (status === 200) {
        return data;
      }

      console.log('Error getting current user group');
      return null;
    } catch (error) {
      toast({
        title: 'Failed to check if user has group or not',
        status: 'error',
        duration: 2500,
        isClosable: true
      });
      return null;
    }
  };

  async function createPage() {
    setLoading(true);
    await Promise.all([getAllTags(), getAllStudents()]);
    setValues({
      id: '',
      projectName: '',
      groupName: '',
      students: [],
      link: '',
      description: '',
      techTags: [],
      semesterTag: '',
      images: [],
      videos: ''
      // groupMembers: []
    });
    setHasBefore(false);
    setLoading(false);
  }

  async function editPage(projectId: any) {
    setLoading(true);
    await Promise.all([getAllTags(), getAllStudents(projectId)]);
    getProject(projectId);

    setHasBefore(true);
    setLoading(false);
  }

  async function checkGroupStudent() {
    const data = await getCurrentUserGroup();
    if (data.group !== null) {
      console.log(data);
      await editPage(data.group.id);
      return;
    }
    createPage();
  }

  async function checkGroupAdmin(id: string | null) {
    if (id === null) {
      createPage();
      return;
    }
    await editPage(id);
  }

  function hasStudents() {
    return values.students.length > 0;
  }

  function hasTechTags() {
    return values.techTags.length > 0;
  }

  function hasSemesterTag() {
    return values.semesterTag.length > 0;
  }

  useEffect(() => {
    casVerifyLogin(cookies, setCookie, removeCookie, login);
  }, []);

  useEffect(() => {
    const projectId = searchParams.get('id');
    console.log('project id is ' + projectId);

    if (user?.role === Role.Student) checkGroupStudent();
    else if (user?.role === Role.Admin) checkGroupAdmin(projectId);

    setLoading(false);
  }, [user]);

  if (loading) return <VtSpinner />;

  if (!user)
    return (
      <Flex flexDirection="column" w="100vw" h="100%">
        <VtHeader />
        <VtHeading>Please login with VT CS CAS to access these features</VtHeading>
        <Button
          justifyContent="center"
          alignSelf="center"
          w={['75%', '50%', '15%']}
          mt="5"
          bg="maroon"
          variant="solid"
          _hover={{ bg: '#a32148' }}
          leftIcon={<Icon as={BiLogIn} />}
          onClick={casLogin}>
          Login
        </Button>
      </Flex>
    );

  return (
    <Form
      onSubmit={async (e) => {
        console.log(values);

        // Verify students, tech tags, and semester fields are not empty
        if (!hasStudents()) {
          toast({
            title: '"Students" is a required field.',
            description: 'Please choose one or more students to associate with this project',
            status: 'error',
            duration: 2500,
            isClosable: true
          });
          return;
        }
        if (!hasSemesterTag()) {
          toast({
            title: '"Semester" is a required field.',
            description: 'Please choose a semester for this project',
            status: 'error',
            duration: 2500,
            isClosable: true
          });
          return;
        }
        if (!hasTechTags()) {
          toast({
            title: '"Tech Stack Tags" is a required field.',
            description: 'Please choose one or more tags to associate with this project',
            status: 'error',
            duration: 2500,
            isClosable: true
          });
          return;
        }

        let includesUser = false;
        defaultStudents.forEach((f) => {
          if (f.value === user.pid) includesUser = true;
        });
        if (!includesUser && user.role === Role.Student) {
          toast({
            title: `Must include yourself when editing / creating a project!`,
            status: 'error',
            duration: 2500,
            isClosable: true
          });
          return;
        }

        // POST to backend
        if (!hasBefore) {
          try {
            const { data, status } = await axiosCredentials(cookies.token).post(
              '/createproject',
              values
            );

            // success:
            if (status === 200) {
              toast({
                title: `Successfully created project ${values.projectName}`,
                status: 'success',
                duration: 2500,
                isClosable: true
              });
              setLoading(true);
              const imageForm = new FormData();
              for (let i = 0; i < values.images.length; i++) {
                imageForm.append(i.toString(), values.images[i]);
              }
              if (user.role === Role.Admin)
                await axiosCredentials(cookies.token).post('/upload?id=' + data.id, imageForm);
              else await axiosCredentials(cookies.token).post('/upload', imageForm);
              toast({
                title: `Successfully uploaded images for project ${values.projectName}`,
                status: 'success',
                duration: 2500,
                isClosable: true
              });

              // redirect to the project page of the newly created project
              // TODO: needs to redirect to /project?id={projectid}
              console.log(data);
              navigate(`/project/${data.id}`);

              return;
            }
            // error:
            toast({
              title: `Creation of project ${values.projectName} unsuccessful.`,
              status: 'error',
              duration: 2500,
              isClosable: true
            });
          } catch (exception) {
            setLoading(false);
            toast({
              title: `Creation of project ${values.projectName} unsuccessful.`,
              status: 'error',
              duration: 2500,
              isClosable: true
            });
          }
        } else {
          try {
            const { data, status } = await axiosCredentials(cookies.token).post(
              '/editproject' + `?id=${values.id}`,
              values
            );

            // success:
            if (status === 200) {
              toast({
                title: `Successfully edited project ${values.projectName}, now uploading images`,
                status: 'success',
                duration: 2500,
                isClosable: true
              });
              setLoading(true);
              const imageForm = new FormData();
              for (let i = 0; i < values.images.length; i++) {
                imageForm.append(i.toString(), values.images[i]);
              }

              if (user.role === Role.Admin)
                await axiosCredentials(cookies.token).post('/upload?id=' + values.id, imageForm);
              else await axiosCredentials(cookies.token).post('/upload', imageForm);
              toast({
                title: `Successfully uploaded images for project ${values.projectName}`,
                status: 'success',
                duration: 2500,
                isClosable: true
              });

              // redirect to the project page of the newly created project
              // TODO: needs to redirect to /project?id={projectid}
              console.log(data);
              navigate(`/project/${values.id}`);

              return;
            }
            // error:
            setLoading(false);
            toast({
              title: `Edit of project ${values.projectName} unsuccessful.`,
              status: 'error',
              duration: 2500,
              isClosable: true
            });
          } catch (exception) {
            setLoading(false);
            toast({
              title: `Edit of project ${values.projectName} unsuccessful.`,
              status: 'error',
              duration: 2500,
              isClosable: true
            });
          }
        }
      }}>
      <Flex flexDirection="column" w="100vw" h="100%" alignItems="center" justifyContent="center">
        <VtHeader />
        <VtHeading>Project {hasBefore ? 'Editing' : 'Submission'}</VtHeading>
        <VtText>
          {hasBefore ? 'Update project information below.' : 'Enter project information below.'}
        </VtText>
        <FormControl pt="8" w={fieldWidth} color="maroon" isRequired>
          <FormLabel>Group Name</FormLabel>
          <Input
            type="text"
            focusBorderColor="maroon"
            borderColor="maroon"
            textColor="black"
            fontSize="11pt"
            value={values.groupName}
            onChange={(e) =>
              setValues((prev) => {
                return { ...prev, groupName: e.target.value };
              })
            }
          />
        </FormControl>
        <FormControl pt="8" w={fieldWidth} color="maroon" isRequired>
          <FormLabel>Students (search by PID)</FormLabel>
          <Select
            // TODO: integrate with backend to get list of students
            options={availableStudents}
            value={defaultStudents}
            // value={}
            isClearable
            isSearchable
            isMulti
            onChange={(e) => {
              const studentpids: string[] = [];
              e.forEach((curr) => {
                studentpids.push(curr.value);
              });
              setDefaultStudents(studentpids.map((e) => ({ value: e, label: e })));
              setValues((prev) => {
                return { ...prev, students: studentpids };
              });
            }}
          />
        </FormControl>
        <FormControl pt="8" w={fieldWidth} color="maroon" isRequired>
          <FormLabel>Project Name</FormLabel>
          <Input
            type="text"
            focusBorderColor="maroon"
            borderColor="maroon"
            textColor="black"
            fontSize="11pt"
            value={values.projectName}
            onChange={(e) =>
              setValues((prev) => {
                return { ...prev, projectName: e.target.value };
              })
            }
          />
        </FormControl>
        <FormControl pt="8" w={fieldWidth} color="maroon" isRequired>
          <FormLabel>Git Repository Link</FormLabel>
          <Input
            type="url"
            focusBorderColor="maroon"
            borderColor="maroon"
            textColor="black"
            fontSize="11pt"
            value={values.link}
            onChange={(e) =>
              setValues((prev) => {
                return { ...prev, link: e.target.value };
              })
            }
          />
        </FormControl>
        <FormControl pt="8" w={fieldWidth} color="maroon" isRequired>
          <FormLabel>Semester</FormLabel>
          <Select
            options={availableSemesterTags}
            isClearable
            isSearchable
            onChange={(e) => {
              const semester = e ? e.value : '';
              setDefaultSemesterTag({ value: semester, label: semester });
              setValues((prev) => {
                return { ...prev, semesterTag: semester ? semester : prev.semesterTag };
              });
            }}
            value={defaultSemesterTag}
          />
        </FormControl>
        <FormControl pt="8" w={fieldWidth} color="maroon" isRequired>
          <FormLabel>Tech Stack Tags</FormLabel>
          <Select
            value={defaultTechTags}
            options={availableTechTags}
            isClearable
            isMulti
            isSearchable
            onChange={(e) => {
              const newTechTags = e.map((e) => e.value);
              setDefaultTechTags(newTechTags.map((e) => ({ value: e, label: e })));
              setValues((prev) => {
                return { ...prev, techTags: newTechTags };
              });
            }}
          />
        </FormControl>
        <FormControl pt="8" w={fieldWidth} color="maroon" isRequired>
          <FormLabel>Description</FormLabel>
          <Textarea
            focusBorderColor="maroon"
            borderColor="maroon"
            textColor="black"
            fontSize="11pt"
            value={values.description}
            size="lg"
            resize="vertical"
            placeholder=""
            maxLength={5000}
            onChange={(e) =>
              setValues((prev) => {
                return { ...prev, description: e.target.value };
              })
            }
          />
        </FormControl>
        <FormControl pt="8" w={fieldWidth} color="maroon">
          <FormLabel>Images (up to {imageLimit})</FormLabel>
          <Input
            type="file"
            multiple
            accept="image/*"
            appearance="none"
            focusBorderColor="maroon"
            borderColor="maroon"
            onChange={(e) => {
              if (e.target.files === undefined || e.target.files === null) return;
              console.log(e.target.files);
              if (values.images.length + e.target.files.length <= imageLimit)
                setValues((prev) => {
                  if (e.target.files !== null)
                    for (let i = 0; i < e.target.files?.length; i++) {
                      const file = e.target.files.item(i);
                      if (
                        file !== null &&
                        prev.images.find((c) => c.name === file.name) === undefined
                      )
                        prev.images.push(file);
                      else {
                        toast({
                          title: 'Failed to add image',
                          description:
                            'The image is invalid, or it shares a name with a previously uploaded image.',
                          status: 'error',
                          duration: 5000,
                          isClosable: true
                        });
                      }
                    }
                  // prev.images.push(...filesList);
                  return { ...prev };
                });
              else {
                // tell user too much images
                toast({
                  title: 'Failed to add image',
                  description: 'Image upload limit (' + imageLimit + ') reached.',
                  status: 'error',
                  duration: 5000,
                  isClosable: true
                });
              }
            }}
          />
        </FormControl>
        <Flex w={fieldWidth} color="maroon" flexDirection="column" alignItems="flex-start">
          {values.images.map((e) => {
            return (
              <Flex flex="1" flexDirection="row" key={e.name} alignItems="center">
                {e.name}
                <IconButton
                  bg="white"
                  icon={<MdCancel />}
                  aria-label={`${e.name} remove`}
                  onClick={() => {
                    setValues((prev) => {
                      const filtered = prev.images.filter((c) => c !== e);
                      return { ...prev, images: filtered };
                    });
                  }}
                />
              </Flex>
            );
          })}
        </Flex>
        <FormControl pt="8" w={fieldWidth} color="maroon">
          <FormLabel>Video Link (YouTube)</FormLabel>
          <Input
            type="url"
            focusBorderColor="maroon"
            borderColor="maroon"
            textColor="black"
            fontSize="11pt"
            value={values.videos}
            onChange={(e) =>
              setValues((prev) => {
                return { ...prev, videos: e.target.value };
              })
            }
          />
        </FormControl>

        {/* <FormControl pt="8" w={ fieldWidth }  color="maroon">
          <FormLabel>Videos (up to {videoLimit})</FormLabel>
          <Input
            type="file"
            multiple
            accept="video/*"
            appearance="none"
            focusBorderColor="maroon"
            borderColor="maroon"
            onChange={(e) => {
              if (e.target.files === undefined || e.target.files === null) return;
              console.log(e.target.files);
              if (values.videos.length + e.target.files.length <= videoLimit)
                setValues((prev) => {
                  if (e.target.files !== null)
                    for (let i = 0; i < e.target.files?.length; i++) {
                      const file = e.target.files.item(i);
                      if (
                        file !== null &&
                        prev.videos.find((c) => c.name === file.name) === undefined
                      )
                        prev.videos.push(file);
                      else {
                        toast({
                          title: 'Failed to add video',
                          description:
                            'The video is invalid, or it shares a name with a previously uploaded video.',
                          status: 'error',
                          duration: 5000,
                          isClosable: true
                        });
                      }
                    }
                  // prev.videos.push(...filesList);
                  return { ...prev };
                });
              else {
                // tell user too many videos
                toast({
                  title: 'Failed to add video',
                  description: 'Video upload limit (' + videoLimit + ') reached.',
                  status: 'error',
                  duration: 5000,
                  isClosable: true
                });
              }
            }}
          />
        </FormControl>
        <Flex w={ fieldWidth }  color="maroon" flexDirection="column" alignItems="flex-start">
          {values.videos.map((e) => {
            return (
              <Flex flex="1" flexDirection="row" key={e.name} alignItems="center">
                {e.name}
                <IconButton
                  bg="white"
                  icon={<MdCancel />}
                  aria-label={`${e.name} remove`}
                  onClick={() => {
                    setValues((prev) => {
                      const filtered = prev.videos.filter((c) => c !== e);
                      return { ...prev, videos: filtered };
                    });
                  }}
                />
              </Flex>
            );
          })}
        </Flex> */}

        <Button my="8" variant="solid" colorScheme="maroon" bg="maroon" type="submit">
          {hasBefore ? 'Save' : 'Submit'}
        </Button>
      </Flex>
    </Form>
  );
}

export default SubmissionPageStudent;
