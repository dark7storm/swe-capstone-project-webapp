import { Button, Flex, Heading, Icon } from '@chakra-ui/react';
import { useContext } from 'react';
import { BiLogIn } from 'react-icons/bi';
import { useParams } from 'react-router-dom';

import VtHeader from '../../components/VtHeader';
import VtHeading from '../../components/VtHeading';
import { UserContext } from '../../context/UserContext';
import Role from '../../enum/Role';
import { casLogin } from '../../services/cas';
import SubmissionPageStudent from './SubmissionPageStudent';

function SubmissionPage() {
  const { user } = useContext(UserContext);

  // guest
  if (!user)
    return (
      <Flex flexDirection="column" w="100vw" h="100%">
        <VtHeader />
        <VtHeading>Please login with VT CS CAS to access these features</VtHeading>
        <Button
          justifyContent="center"
          alignSelf="center"
          w={['75%', '50%', '15%']}
          mt="5"
          bg="maroon"
          variant="solid"
          _hover={{ bg: '#a32148' }}
          leftIcon={<Icon as={BiLogIn} />}
          onClick={casLogin}>
          Login
        </Button>
      </Flex>
    );

  if (user?.role === Role.Student) {
    // here check if user is in group or not

    // here check if group already has submission or not
    // also need to check if the link user is part of the group
    return <SubmissionPageStudent />;
  }
  if (user?.role === Role.Admin) {
    return <SubmissionPageStudent />;
  }

  return (
    <Flex flexDirection="column" w="100vw" h="100%">
      <VtHeader />
      <VtHeading>Please login with VT CS CAS to access these features</VtHeading>
      <Button
        justifyContent="center"
        alignSelf="center"
        w={['75%', '50%', '15%']}
        mt="5"
        bg="maroon"
        variant="solid"
        _hover={{ bg: '#a32148' }}
        leftIcon={<Icon as={BiLogIn} />}
        onClick={casLogin}>
        Login
      </Button>
    </Flex>
  );
}

export default SubmissionPage;
