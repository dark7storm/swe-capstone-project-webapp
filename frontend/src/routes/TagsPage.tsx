import {
  Button,
  Checkbox,
  Flex,
  FormLabel,
  Icon,
  Input,
  SimpleGrid,
  useToast
} from '@chakra-ui/react';
import { useContext, useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { BiLogIn } from 'react-icons/bi';
import { Form } from 'react-router-dom';

import { axiosCredentials, axiosNoCredentials } from '../axios';
import VtHeader from '../components/VtHeader';
import VtHeading from '../components/VtHeading';
import VtSpinner from '../components/VtSpinner';
import VtText from '../components/VtText';
import { UserContext } from '../context/UserContext';
import Role from '../enum/Role';
import { casLogin, casVerifyLogin } from '../services/cas';
import Error from './404';

interface AllTagsProps {
  techTags: string[];
  semesterTags: string[];
}

interface TechTagsProps {
  techTags: string[];
}

interface SemesterTagsProps {
  semesterTags: string[];
}

interface FormData {
  toDeleteTech: string[];
  toDeleteSemester: string[];
  techTags: string[];
  semesterTags: string[];
}
function TagsPage() {
  const toast = useToast();
  const [cookies, setCookie, removeCookie] = useCookies(['token']);

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const { user, login } = useContext(UserContext);
  const [values, setValues] = useState<FormData>({
    toDeleteTech: [],
    toDeleteSemester: [],
    techTags: [],
    semesterTags: []
  });
  const [techInput, setTechInput] = useState('');
  const [semesterInput, setSemesterInput] = useState('');

  const getAllTags = async () => {
    const tagsFailedToLoad = () => {
      toast({
        title: 'Tags failed to load',
        status: 'error',
        duration: 2500,
        isClosable: true
      });

      setValues({
        toDeleteTech: [],
        toDeleteSemester: [],
        techTags: [],
        semesterTags: []
      });

      setError(true);
      setLoading(false);
    };

    try {
      const { data, status } = await axiosNoCredentials().get<AllTagsProps>('/alltags');
      if (status === 200) {
        setValues({
          toDeleteTech: [],
          toDeleteSemester: [],
          techTags: data.techTags.map((e) => e),
          semesterTags: data.semesterTags.map((e) => e)
        });
        setLoading(false);
        return;
      }
      tagsFailedToLoad();
    } catch (e) {
      tagsFailedToLoad();
    }
  };

  useEffect(() => {
    // fetch current semester and tech tags
    casVerifyLogin(cookies, setCookie, removeCookie, login);
  }, []);

  useEffect(() => {
    getAllTags();
  }, [user]);

  // guest
  if (!user)
    return (
      <Flex flexDirection="column" w="100vw" h="100%">
        <VtHeader />
        <VtHeading>Please login with VT CS CAS to access these features</VtHeading>
        <Button
          justifyContent="center"
          alignSelf="center"
          w={['75%', '50%', '15%']}
          mt="5"
          bg="maroon"
          variant="solid"
          _hover={{ bg: '#a32148' }}
          leftIcon={<Icon as={BiLogIn} />}
          onClick={casLogin}>
          Login
        </Button>
      </Flex>
    );

  if (user?.role !== Role.Admin) return <Error />;

  if (loading) return <VtSpinner />;

  if (error)
    return (
      <Flex flexDirection="column" alignItems="center" w="100vw" h="100%" justifyContent="center">
        <VtHeader />
        <VtHeading>Failed to load data for page</VtHeading>
      </Flex>
    );

  return (
    <Form
      onSubmit={() => {
        console.log(values);
      }}>
      <Flex flexDirection="column" alignItems="center" w="100vw" h="100%" justifyContent="center">
        <VtHeader />
        <VtHeading>Tags Edit</VtHeading>
        <VtText>
          To add, enter new tag and press {"'Add'"} button next to input. To remove, click checkbox
          and then press {"'Remove'"} button.
        </VtText>
        <SimpleGrid
          my={8}
          row={3}
          spacing={6}
          color="maroon"
          // flexDirection="row"
          justifySelf="flex-start"
          alignItems="flex-start">
          <FormLabel fontSize={['lg', 'xl', '2xl']}>Tech Tags</FormLabel>
          <Flex flexDirection={['column', 'row']} gap="4" w="80vw">
            <Input
              type="text"
              borderColor="maroon"
              focusBorderColor="maroon"
              value={techInput}
              onChange={(e) => setTechInput(e.target.value)}
              _hover={{ borderColor: 'maroon' }}
            />
            <Button
              onClick={async (e) => {
                if (techInput.length === 0) {
                  toast({
                    title: 'No empty tags allowed',
                    description: 'Please enter a valid string in input field',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                  setTechInput('');
                  return;
                }

                if (!values.techTags.every((c) => c.toLowerCase() !== techInput.toLowerCase())) {
                  toast({
                    title: 'Tag already exists',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                  setTechInput('');
                  return;
                }

                try {
                  const { status } = await axiosCredentials(cookies.token).post('/tags', {
                    tag: techInput
                  });

                  // techTags.push(techInput);
                  if (status === 200) {
                    toast({
                      title: `Added ${techInput}`,
                      status: 'success',
                      duration: 2500,
                      isClosable: true
                    });
                    setValues({ ...values, techTags: [...values.techTags, techInput] });
                    setTechInput('');
                    return;
                  }

                  toast({
                    title: `Added ${techInput} not successful, something happened with server`,
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                } catch (e) {
                  toast({
                    title: `Added ${techInput} not successful, something happened with server or you are not an admin`,
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                }
              }}>
              Add
            </Button>
          </Flex>
          <Flex flexDirection={['column', 'row']} w="80vw">
            <SimpleGrid py={4} scrollBehavior="auto" columns={[2, 3, 5, 10]} spacing={5}>
              {values.techTags.map((e) => {
                return (
                  <Checkbox
                    colorScheme="red"
                    border="maroon"
                    key={e}
                    onChange={(t) => {
                      console.log(t.target.checked);
                      if (t.target.checked) {
                        setValues((p) => {
                          p.toDeleteTech.push(e);
                          return { ...p };
                        });
                        return;
                      }

                      setValues((p) => {
                        p.toDeleteTech = p.toDeleteTech.filter((f) => f !== e);
                        return { ...p };
                      });
                    }}>
                    {e}
                  </Checkbox>
                );
              })}
            </SimpleGrid>
            <Button
              alignSelf="center"
              onClick={async (e) => {
                // tell server to remove
                const toDeleteTech = values.toDeleteTech;
                if (toDeleteTech.length === 0) {
                  toast({
                    title: 'No tags were ticked',
                    description: 'Please tick tags to remove',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                  return;
                }
                try {
                  const { data, status } = await axiosCredentials(
                    cookies.token
                  ).delete<TechTagsProps>('/tags', {
                    data: {
                      tag: toDeleteTech
                    }
                  });

                  if (status === 200) {
                    toast({
                      title: 'Ticked tags are deleted',
                      status: 'success',
                      duration: 2500,
                      isClosable: true
                    });
                    setValues({ ...values, toDeleteTech: [], techTags: data.techTags });
                  }
                } catch (e) {
                  toast({
                    title:
                      'Ticked tags are NOT deleted, something happened with server or you are not an admin',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                }
                // if error, use toast to say
              }}>
              Remove
            </Button>
          </Flex>
        </SimpleGrid>
        <SimpleGrid
          row={3}
          spacing={6}
          mt="8"
          color="maroon"
          // flexDirection="row"
          // alignItems="center"
          justifyContent="center">
          <FormLabel fontSize={['lg', 'xl', '2xl']}>Semester Tags</FormLabel>
          <Flex flexDirection={['column', 'row']} gap="4" w="80vw">
            <Input
              type="text"
              borderColor="maroon"
              focusBorderColor="maroon"
              value={semesterInput}
              onChange={(e) => setSemesterInput(e.target.value)}
              _hover={{ borderColor: 'maroon' }}
            />
            <Button
              onClick={async (e) => {
                if (semesterInput.length === 0) {
                  toast({
                    title: 'No empty tags allowed',
                    description: 'Please enter a valid string in input field',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                  setSemesterInput('');
                  return;
                }

                if (
                  !values.semesterTags.every((c) => c.toLowerCase() !== semesterInput.toLowerCase())
                ) {
                  toast({
                    title: 'Tag already exists',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                  setSemesterInput('');
                  return;
                }

                try {
                  const { status } = await axiosCredentials(cookies.token).post('/semestertags', {
                    semester: semesterInput
                  });

                  if (status === 200) {
                    toast({
                      title: `Added ${semesterInput}`,
                      status: 'success',
                      duration: 2500,
                      isClosable: true
                    });
                    setValues({ ...values, semesterTags: [...values.semesterTags, semesterInput] });
                    setSemesterInput('');
                    return;
                  }

                  toast({
                    title: `Added ${semesterInput} not successful, something happened with server`,
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                } catch (e) {
                  toast({
                    title: `Added ${semesterInput} not successful, something happened with server or you are not an admin`,
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                }
              }}>
              Add
            </Button>
          </Flex>
          <Flex flexDirection={['column', 'row']} w="80vw">
            <SimpleGrid py={4} scrollBehavior="auto" columns={[2, 3, 5, 10]} spacing={5}>
              {values.semesterTags.map((e) => {
                return (
                  <Checkbox
                    colorScheme="red"
                    border="maroon"
                    key={e}
                    onChange={(t) => {
                      console.log(t.target.checked);
                      if (t.target.checked) {
                        setValues((p) => {
                          p.toDeleteSemester.push(e);
                          return { ...p };
                        });
                        return;
                      }

                      setValues((p) => {
                        p.toDeleteSemester = p.toDeleteSemester.filter((f) => f !== e);
                        return { ...p };
                      });
                    }}>
                    {e}
                  </Checkbox>
                );
              })}
            </SimpleGrid>
            <Button
              mb={4}
              alignSelf="center"
              onClick={async (e) => {
                // tell server to remove
                // tell server to remove
                const toDeleteSemester = values.toDeleteSemester;
                if (toDeleteSemester.length === 0) {
                  toast({
                    title: 'No tags were ticked',
                    description: 'Please tick tags to remove',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                  return;
                }
                try {
                  const { data, status } = await axiosCredentials(
                    cookies.token
                  ).delete<SemesterTagsProps>('/semestertags', {
                    data: {
                      semester: toDeleteSemester
                    }
                  });

                  if (status === 200) {
                    toast({
                      title: 'Ticked tags are deleted',
                      status: 'success',
                      duration: 2500,
                      isClosable: true
                    });
                    setValues({ ...values, toDeleteSemester: [], semesterTags: data.semesterTags });
                  } else {
                    toast({
                      title:
                        'Ticked tags are NOT deleted, something happened with server or you are not an admin',
                      status: 'error',
                      duration: 2500,
                      isClosable: true
                    });
                  }
                } catch (e) {
                  toast({
                    title:
                      'Ticked tags are NOT deleted, something happened with server or you are not an admin',
                    status: 'error',
                    duration: 2500,
                    isClosable: true
                  });
                }
              }}>
              Remove
            </Button>
          </Flex>
        </SimpleGrid>
        {/* <Button mt="8" variant="solid" colorScheme="maroon" bg="maroon" type="submit">
          Submit
        </Button> */}
      </Flex>
    </Form>
  );
}

export default TagsPage;
