// import '../App.css';

import './Home.css';

import {
  Box,
  Button,
  Container,
  Flex,
  FormLabel,
  Grid,
  GridItem,
  Icon,
  Input,
  SimpleGrid,
  useColorMode,
  useMergeRefs,
  useToast
} from '@chakra-ui/react';
import axios from 'axios';
import debounce from 'lodash.debounce';
import { ChangeEvent, useCallback, useContext, useEffect, useRef, useState } from 'react';
import { BiLogIn } from 'react-icons/bi';
import ReactLoading from 'react-loading';
import ReactPaginate from 'react-paginate';
import Select, { GroupBase, OptionsOrGroups } from 'react-select';

import { axiosCredentials, axiosNoCredentials } from '../axios';
import VtHeader from '../components/VtHeader';
import VtHeading from '../components/VtHeading';
import VtProjectCard from '../components/VtProjectCard';
import VtText from '../components/VtText';
import { UserContext } from '../context/UserContext';
import { ProjectCard } from '../interfaces/ProjectCards';
import { casLogin } from '../services/cas';

const cardsPerPage = 8;

interface filterProps {
  value: string;
  label: string;
}

export default function HomePage(): JSX.Element {
  const { user } = useContext(UserContext);
  const [semester, setSemester] = useState<string[]>([]);
  const [tech, setTech] = useState<string[]>([]);
  const [text, setText] = useState<string>('');
  // const listInnerRef = useRef() as React.MutableRefObject<HTMLDivElement>;
  // const finalRef = useMergeRefs(listInnerRef);
  const [cards, setCards] = useState<ProjectCard[]>();
  const [pageCount, setPageCount] = useState(1);
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [pageOffset, setPageOffset] = useState(0);
  const toast = useToast();

  const [semesterTags, setSemesterTags] = useState<filterProps[]>([]);
  const [techTags, setTechTags] = useState<filterProps[]>([]);

  const [loading, setLoading] = useState(false);

  const grabProjects = async () => {
    try {
      console.log('Grab projects start');
      setLoading(true);
      const { data, status } = await axiosNoCredentials().post(
        `/filterprojects?page=${pageOffset}`,
        {
          tags: tech,
          semester: semester,
          name: text
        }
      );
      console.log(data);
      if (status === 200) {
        setPageCount(data['max_pages'] + 1);
        setCards(data['projects']);
        setPageOffset(parseInt(data['page']));
      } else {
        toast({
          description: 'Something went wrong with loading projects',
          status: 'error',
          duration: 2000
        });
      }
    } catch (e) {
      toast({
        description: 'Something went wrong with loading projects',
        status: 'error',
        duration: 2000
      });
    }

    setLoading(false);
  };

  const grabTags = async () => {
    try {
      const { data, status } = await axiosNoCredentials().get(`/alltags`);

      if (status === 200) {
        setSemesterTags(data['semesterTags'].map((e: any) => ({ value: e, label: e })));
        setTechTags(data['techTags'].map((e: any) => ({ value: e, label: e })));
        // setPageCount(data['max_pages'] + 1);
        // setCards(data['projects']);
        // setPageOffset(parseInt(data['page']));
      } else {
        toast({
          description: 'Something went wrong with loading projects',
          status: 'error',
          duration: 2000
        });
      }
    } catch (e) {
      toast({
        description: 'Something went wrong with loading projects',
        status: 'error',
        duration: 2000
      });
    }
  };

  useEffect(() => {
    // initial processing
    // grab all projects
    // grab tags to be able to filter
    console.log(user);
    if (user) {
      grabProjects();
      grabTags();
    }
  }, []);

  useEffect(() => {
    // initial processing
    // grab all projects
    // grab tags to be able to filter

    if (user) {
      grabProjects();
      grabTags();
    }
  }, [user]);

  useEffect(() => {
    // Fetch items from another resources.
    if (user) grabProjects();
  }, [pageOffset, cardsPerPage]);

  // Invoke when user click to request another page.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handlePageClick = (event: any) => {
    // console.log(`User requested page number ${event.selected}`);
    setPageOffset(event.selected);
  };

  if (!user)
    return (
      <Flex flexDirection="column" w="100vw" h="100%">
        <VtHeader />
        <VtHeading>Please login with VT CS CAS to access these features</VtHeading>
        <Button
          justifyContent="center"
          alignSelf="center"
          w={['75%', '50%', '15%']}
          mt="5"
          bg="maroon"
          textColor="white"
          variant="solid"
          _hover={{ bg: '#a32148' }}
          leftIcon={<Icon as={BiLogIn} />}
          onClick={casLogin}>
          Login
        </Button>
      </Flex>
    );

  // console.log(`${import.meta.env.VITE_CAS_URL}?next=${encodeURIComponent(window.location.href)}`);
  // console.log(window.location.href);
  return (
    <>
      <Container w="100vw" h="100%" overflowY={['auto', 'auto', 'hidden']} m="0" maxW="none" p="0">
        <VtHeader />
        <Flex
          // w={['100vw', 'auto']}
          flexDirection={['column', 'column', 'row']}
          justifyContent="center"
          alignContent="center"
          alignItems="center"
          justifyItems="center"
          p="4">
          <Container>
            <FormLabel mt="4">Project</FormLabel>
            <Input
              type="text"
              borderColor="maroon"
              focusBorderColor="maroon"
              value={text}
              onChange={(e) => setText(e.target.value)}
            />
          </Container>
          <Container>
            <FormLabel mt="4">Semester</FormLabel>
            <Select
              options={semesterTags}
              isClearable
              isSearchable
              isMulti
              onChange={(e) => {
                setSemester(e.map((l) => l.value));
              }}
            />
          </Container>
          <Container>
            <FormLabel mt="4">Tech stack</FormLabel>

            <Select
              options={techTags}
              isClearable
              isSearchable
              isMulti
              onChange={(e) => {
                setTech(e.map((l) => l.value));
              }}
            />
          </Container>
          <Button
            transform="translate(0%, 50%)"
            m={['4', '4', '8']}
            px={['4', '4', '8']}
            py={['2', '2', '4']}
            onClick={async (e) => {
              // tell server to find projects with given filter
              // console.log(tech);
              // console.log(semester);
              // console.log(text);
              try {
                const { data, status } = await axiosNoCredentials().post(
                  `/filterprojects?page=${pageOffset}`,
                  {
                    tags: tech,
                    semester: semester,
                    text: text
                  }
                );

                if (status === 200) {
                  // console.log(data);
                  setPageCount(data['max_pages'] + 1);
                  setCards(data['projects']);
                  setPageOffset(parseInt(data['page']));
                } else {
                  toast({
                    description: 'Something went wrong with loading projects',
                    status: 'error',
                    duration: 2000
                  });
                }
              } catch (e) {
                toast({
                  description: 'Something went wrong with loading projects',
                  status: 'error',
                  duration: 2000
                });
              }
            }}>
            Search
          </Button>
        </Flex>
        <GridItem colSpan={[null, null, 2, 3]}>
          {loading && (
            <Flex
              flexDirection="row"
              alignContent="center"
              justifyContent="center"
              alignItems="center"
              justifyItems="center"
              w={['auto']}
              p="4">
              <ReactLoading color="#861F41" height={'10%'} width={'10%'} type={'spin'} />
            </Flex>
          )}
          <Grid
            p="4"
            templateColumns={[null, 'repeat(2, 1fr)', 'repeat(3, 1fr)', 'repeat(4, 1fr)']}>
            {cards &&
              !loading &&
              cards.map((e, i) => {
                return (
                  <GridItem key={i}>
                    <VtProjectCard project={e} />
                  </GridItem>
                );
              })}
          </Grid>
          {cards?.length === 0 && (
            <Flex
              flexDirection="row"
              alignContent="center"
              justifyContent="center"
              alignItems="center"
              justifyItems="center"
              w={['auto']}
              p="4">
              <VtText size={['md', 'lg', '2xl', '2xl']} as="h1">
                No project found with given filters!
              </VtText>
            </Flex>
          )}
        </GridItem>
      </Container>
      <Flex p="4" overscrollBehaviorX="auto" w="100%">
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          previousLabel="<"
          className="paginate-row"
          pageClassName="paginate-item"
          activeClassName="paginate-item-active"
          // renderOnZeroPageCount={null}
        />
      </Flex>
    </>
  );
}
