enum Role {
  Student = 'Student',
  Admin = 'Admin'
}

export default Role;
