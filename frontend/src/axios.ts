import axios from 'axios';

// for local dev
// const baseURL = 'http://localhost:3999';

const baseURL = 'https://havensprings.discovery.cs.vt.edu';

export const axiosCredentials = (token: string) => {
  return axios.create({
    baseURL: baseURL,
    withCredentials: true,
    headers: {
      'Access-Control-Allow-Origin': baseURL,
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Credentials': 'true',
      'X-XSRF-TOKEN': token
    }
  });
};

export const axiosNoCredentials = () => {
  return axios.create({
    baseURL: baseURL,
    headers: {
      'Access-Control-Allow-Origin': baseURL,
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Credentials': 'true'
    }
  });
};
