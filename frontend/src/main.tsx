// import './index.css';

import './index.css';

import { ChakraProvider } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { CookiesProvider, useCookies } from 'react-cookie';
import ReactDOM from 'react-dom/client';
import { BsFillPersonXFill } from 'react-icons/bs';
import { FaFile, FaHome, FaPaperPlane } from 'react-icons/fa';
import { MdAddCircleOutline } from 'react-icons/md';
import { RiGroupFill } from 'react-icons/ri';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import VtHeader from './components/VtHeader';
import { UserContext } from './context/UserContext';
import Role from './enum/Role';
import User from './interfaces/User';
import Error from './routes/404';
// import GroupPage from './routes/GroupPage/GroupPage';
import HomePage from './routes/HomePage';
import ProjectPage from './routes/ProjectPage/ProjectPage';
import SubmissionPage from './routes/SubmissionPage/SubmissionPage';
import TagsPage from './routes/TagsPage';
import { verifyLogin } from './services/cas';
import theme from './theme';

export const userRoutes = [
  {
    path: '/',
    element: <HomePage />,
    errorElement: <Error />,
    id: 'Home',
    icon: FaHome
  },
  {
    path: '/submission',
    element: <SubmissionPage />,
    errorElement: <Error />,
    id: 'Submission',
    icon: FaPaperPlane
  },
  {
    path: '/project/:id',
    element: <ProjectPage />,
    errorElement: <Error />,
    id: 'ProjectId',
    icon: FaFile
  },
  {
    path: '/project',
    element: <ProjectPage />,
    errorElement: <Error />,
    id: 'View Project',
    icon: FaFile
  }
];

export const adminRoutes = [
  {
    path: '/',
    element: <HomePage />,
    errorElement: <Error />,
    id: 'Home',
    icon: FaHome
  },
  {
    path: '/tags/edit',
    element: <TagsPage />,
    errorElement: <Error />,
    id: 'Edit Tags',
    icon: RiGroupFill
  },
  {
    path: '/project/:id',
    element: <ProjectPage />,
    errorElement: <Error />,
    id: 'Project',
    icon: FaFile
  },
  {
    path: '/submission',
    element: <SubmissionPage />,
    errorElement: <Error />,
    id: 'Create Project',
    icon: MdAddCircleOutline
  }
];

export const guestRoutes = [
  {
    path: '/',
    element: <HomePage />,
    errorElement: <Error />,
    id: 'Home',
    icon: BsFillPersonXFill
  }
];

const userRouter = createBrowserRouter(userRoutes);
const adminRouter = createBrowserRouter(adminRoutes);

function App() {
  // TODO remove the default after finish implement auth
  const [user, setUser] = useState<User | undefined>();
  const login = (u: User) => setUser(u);
  const logout = () => setUser(undefined);
  const [cookies, setCookie, removeCookie] = useCookies(['token']);
  const [routes, setRoutes] = useState(guestRoutes);

  useEffect(() => {
    // here we check for every App render if
    // user is saved at local storage
    // if is, verify its true with flask (maybe)
    // else if check with cas
    // else not logged in
    const pageLoad = () => {
      const res = verifyLogin(cookies, setCookie, removeCookie);

      if (res) {
        console.log(res);
        login(res);
      }
    };
    pageLoad();
  }, []);

  useEffect(() => {
    if (user?.role === Role.Admin) setRoutes(adminRoutes);
    else if (user?.role === Role.Student) setRoutes(userRoutes);
    else setRoutes(guestRoutes);
  }, [user]);

  return (
    <CookiesProvider>
      <ChakraProvider theme={theme}>
        <UserContext.Provider value={{ user, login, logout }}>
          <RouterProvider router={user?.role === Role.Student ? userRouter : adminRouter} />
        </UserContext.Provider>
      </ChakraProvider>
    </CookiesProvider>
  );
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
